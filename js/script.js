var CORE = CORE || {};
CORE.code = (function (j) {

    var init = function () {
        elip();
        slidingDrawer();
        scrollDown();
        testimonials();
        clientLogo();
        clientCommendation();
        onwindow();
        primaryMenu();
        sidemenu();
        _sideMenu();
        masonry();
        commentvalidate();
        j.stellar();
        swipeBox();
        searchBox();
        scrollToTop();
        linkContent();
        singleEvent();
        serviceSlider();
    };

    var _window = j(window);
    var _body = j('body');
    var primaryNav = j('.primary-nav');
    var drawer = j('.sliding-drawer');
    var sidebarmenu = j('.sidebar-navmenu');
    var photogallery = function (obj) {
        var _body = j('body');
        j('html,body').css({'overflow':'hidden'});
        _body.append('<div class="photo-gallery-json" style="display:none;"></div>');
        var _photohtml = '<a href="#close-gallery" class="close-gallery"></a><div class="container"><div class="row">';
        for(var i = 0;i<obj.length;i++){
            _photohtml += '<div class="col-md-3 col-sm-4 gallery-item"><a href="'+obj[i].large_photo+'" class="gallery-link" title="'+obj[i].gallery_caption+'"><img  class="gallery-pop-image" src="'+obj[i].thumbnail+'"/><span class="photo-cap">'+obj[i].gallery_caption+'</span></a></div>';
        }
        _photohtml += '<div/><div/>';
        _body.find('.photo-gallery-json').html(_photohtml).fadeIn(500);
        _body.find('.photo-gallery-json').find('img').load(function () {
            j(this).addClass('image-loaded');
        }).each(function () {
            if (this.complete) {
                j(this).load();
            }
        });
        _body.find(".photo-cap").dotdotdot({
            ellipsis: '...',
            watch: "window"
        });
        j(window).on('keydown keypress', function (e) {
            var code = (e.keyCode || e.which);
            if (code === 27) {
                _body.find('.photo-gallery-json').fadeOut(500, function () {
                    j('html,body').removeAttr('style');
                    _body.find('.photo-gallery-json').remove();
                });
            }
        });
        _body.find('.photo-gallery-json .close-gallery').unbind().bind('click',function(e){
            e.preventDefault();
            _body.find('.photo-gallery-json').fadeOut(500,function(){
                j('html,body').removeAttr('style');
                _body.find('.photo-gallery-json').remove();
            });
        });
        j('.gallery-link').swipebox();
    };
    var serviceSlider = function () {
        j.fn.eSlider = function (options) {
            var settings = j.extend({
                animSpeed: 10000,
                outSpeed: 2000,
                animStyle: 'fade'
            }, options);
            return this.each(function () {
                var _window = j(window);
                var _slider = j(this);
                var getFirstImgSize = function (_slider) {
                    var _width = _slider.width();
                    _slider.find('li').css({width: _width});
                    var _height = _slider.find('li:nth-child(2)');
                    _slider.css({height: _height.height()});
                    _slider.animate({opacity: 1}, 1000, function () {
                        var _this = j(this);
                        _this.css({opacity: ''}).addClass('act');
                    });
                };
                var startRotate = function (_slider) {
                    var _height = _slider.find('li:nth-child(2)');
                    _slider.css({height: _height.height()});
                    if (settings.animStyle === 'fade') {
                        _slider.find('li').last().fadeOut(settings.outSpeed, function () {
                            var _this = j(this);
                            _this.css({left: '', opacity: '', display: ''});
                            var _clone = (_this.clone());
                            _this.remove();
                            _slider.prepend(_clone);
                        });
                    } else if (settings.animStyle === 'slideLeft') {
                        _slider.find('li').last().animate({opacity: 0, left: '-100%'}, settings.outSpeed, function () {
                            var _this = j(this);
                            _this.css({left: '', opacity: '', display: ''});
                            var _clone = (_this.clone());
                            _this.remove();
                            _slider.prepend(_clone);
                        });
                    } else if (settings.animStyle === 'slideRight') {
                        _slider.find('li').last().animate({opacity: 0, left: '100%'}, settings.outSpeed, function () {
                            var _this = j(this);
                            _this.css({left: '', opacity: '', display: ''});
                            var _clone = (_this.clone());
                            _this.remove();
                            _slider.prepend(_clone);
                        });
                    } else {
                        _slider.find('li').last().fadeOut(settings.outSpeed, function () {
                            var _this = j(this);
                            _this.css({left: '', opacity: '', display: ''});
                            var _clone = (_this.clone());
                            _this.remove();
                            _slider.prepend(_clone);
                        });
                    }
                    setTimeout(function () {
                        startRotate(_slider);
                    }, settings.animSpeed);
                };
                _window.on('load resize', function () {
                    getFirstImgSize(_slider);
                });
                setTimeout(function () {
                    startRotate(_slider);
                }, settings.animSpeed);
            });
        };
        if (j('.ed-slider').length) {
            j('.ed-slider').each(function () {
                var _this = j(this);
                _this.find('ul').eSlider();
            });
        }
    };


    _window.load(function () {
        _body.find('.loader').fadeOut(200);
    });
    var swipeBox = function () {
        _body.find('.loader').click(function () {
            j(this).fadeOut(200);
        });
        j('.cntn-page a img').parent('a').addClass('swipebox');
        j('.swipebox').swipebox({
            useCSS: true,
            useSVG: true,
            initialIndexOnArray: 0,
            hideCloseButtonOnMobile: false,
            hideBarsDelay: 3000,
            videoMaxWidth: 1140,
            beforeOpen: function () {
            },
            afterOpen: null,
            afterClose: function () {
            },
            loopAtEnd: false
        });
    };
    var elip = function () {
        j(".ellipsis").dotdotdot({
            ellipsis: '...',
            watch: "window"
        });
    };
    var singleEvent = function () {
        j('._event_container .pull-right a').click(function (e) {
            e.preventDefault();
            var _this = j(this);
            var width = 400;
            var height = 400;
            window.open(_this.attr('href'), 'share', "width=" + width + ", height=" + height + ", top=" + ((screen.height) / 2 - (height / 2)) + ", left=" + (screen.width / 2 - (width / 2)));
        });
    };
    var linkContent = function () {
        j('.vc_column-inner a').click(function () {
            var _this = j(this);
            var _hash = _this.attr('href').indexOf("#");
            var _target = _this.attr('target');
            if (_hash >= 0 && _target === '_blank') {
                j(_this.attr('href')).fadeIn(1000, function () {
                    j(_this.attr('href')).stop().animate({backgroundColor: '#F9F2B2'}, 200, function () {
                        j(_this.attr('href')).stop().animate({backgroundColor: '#FFFFFF'}, 200);
                    });
                });
                return false;
            }
        });
    };
    var searchBox = function () {
        j('.search-bar').click(function () {
            j('.search-box').modal('show');
            setTimeout(function () {
                j('.search-box').find('.form-control').focus();
            }, 500);

        });
        j('.modal-search').validate({
            rules: {
                s: 'required'
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.validElements(), function (index, element) {
                    var $element = j(element);
                    $element.parent().removeClass("has-error");
                    $element.data("title", "").tooltip("destroy");
                });
                j.each(errorList, function (index, error) {
                    var $element = j(error.element);
                    $element.parent().addClass("has-error");
                    $element.tooltip("destroy").data("title", error.message).tooltip();
                });
            }
        });
        j('.widget-search').validate({
            rules: {
                s: 'required'
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.validElements(), function (index, element) {
                    var $element = j(element);
                    $element.parent().removeClass("has-error");
                    $element.data("title", "").tooltip("destroy");
                });
                j.each(errorList, function (index, error) {
                    var $element = j(error.element);
                    $element.parent().addClass("has-error");
                    $element.tooltip("destroy").data("title", error.message).tooltip();
                });
            }
        });
    };
    var primaryMenu = function () {
        primaryNav.find('a').removeAttr('title');
        primaryNav.find('.dropdown').hover(function () {
            j(this).find('.dropdown-menu').hide().stop(true, true).slideDown(200, 'easeInOutBack');
        }, function () {
            j(this).find('.dropdown-menu').stop(true, true).slideUp(200, 'easeInOutBack', function () {
                j(this).removeAttr('style');
            });
        });
        primaryNav.find('.collapse a').click(function () {
            var _this = j(this);
            window.location = _this.attr('href');
        });
    };
    var sidemenu = function () {
        sidebarmenu.find('.dropdown').click(function () {
            var _this = j(this);
            if (_this.hasClass('open') === false) {
                _this.find('.dropdown-menu').hide().stop(true, true).slideDown(200, 'easeInOutBack', function () {
                    j(this).removeAttr('style');
                });
            } else {
                _this.find('.dropdown-menu').stop(true, true).slideUp(200, 'easeInOutBack', function () {
                    j(this).removeAttr('style');
                });
            }
        });
    };
    var scrollDown = function () {
        _window.on('load scroll', function () {
            var _this = j(this);
            var top = (_this.scrollTop());
            if (top > 80) {
                primaryNav.addClass('scrolldown');
            } else {
                primaryNav.removeClass('scrolldown');
            }
        });
    };
    var onwindow = function () {
        _window.on('resize load', function () {
            var _width = j(this).width();
            if (_width < 768) {
                _body.removeClass('fixed-top');
                primaryNav.removeClass('navbar-fixed-top');
            } else {
                _body.addClass('fixed-top');
                primaryNav.addClass('navbar-fixed-top');
            }
        });
    };
    var slidingDrawer = function () {
        var sddrawer = j('.drawer-content');
        primaryNav.find('.navbar-toggle').click(function () {
            if (drawer.hasClass('dopen') === false) {
                _body.addClass('mobile-open');
                drawer.animate({right: '80%'}, 300, 'easeInOutQuint', function () {
                    drawer.addClass('dopen');
                    drawer.removeAttr('style');
                });
            } else {
                drawer.animate({right: '0%'}, 300, 'easeInOutQuint', function () {
                    drawer.removeClass('dopen');
                    drawer.removeAttr('style');
                    _body.removeClass('mobile-open');
                });
            }
        });
    };

    var _sideMenu = function () {
        var _drawerscroll = j('.drawer-scroll');
        var main = j(".sidemenu li");
        var slideSpeed = 300;
        main.each(function () {
            var elm = j(this);
            if (elm.find("ul").length !== 0) {
                elm.append('<i class="fa fa-chevron-down"></i>');
            }
        });
        var slimScrll = function () {
            _drawerscroll.slimScroll({alwaysVisible: true});
        };
        slimScrll();
        main.find("a").click(function (e) {
            var clicked = j(e.target);
            var elm = j(this);
            var obj = elm.parent();
            if (obj.find("ul").length !== 0) {
                if (obj.find("ul").first().is(":visible") === false) {
                    if (clicked.parents("li").length < 2) {
                        main.find("ul").slideUp(slideSpeed, function () {
                        });
                        main.removeClass("nav-active");
                        obj.addClass("nav-active");
                    } else {
                        main.find("ul li").removeClass("nav-active");
                        obj.addClass("nav-active");
                        main.find("ul li ul").slideUp(slideSpeed, function () {
                            slimScrll();
                        });
                    }
                    obj.find("ul").first().slideDown(slideSpeed, function () {
                        slimScrll();
                    });
                } else {
                    obj.find("ul").first().slideUp(slideSpeed, function () {
                        slimScrll();
                    });
                    obj.removeClass("nav-active");
                }
                return false;
            } else {
                drawer.animate({right: '0%'}, 300, 'easeInOutQuint', function () {
                    drawer.removeClass('dopen');
                    drawer.removeAttr('style');
                    _body.removeClass('mobile-open');
                });
            }
        });
    };



    var testimonials = function () {
        var _container = j('.comment-box .row');
        var response = function () {
            _container.masonry({
                itemSelector: '.col_'
            });
        };
        response();
        _container.find('img').load(function () {
            response();
            j(this).css({'display': 'block'}).animate({'opacity': '1'}, 1500);
        }).each(function () {
            if (this.complete) {
                j(this).load();
            }
        });
    };

    var clientLogo = function () {
        var logo = j(".client-logos");
        logo.owlCarousel({
            items: logo.attr('data-item'),
            lazyContent: true,
            afterInit: function () {
                j(this).show();
            },
            loop: true,
            margin: 0,
            autoPlay: 8000,
            responsiveRefreshRate: true,
            responsiveClass: true
        });
        logo.find('img').each(function () {
            var _alt = j(this);
            var _url = _alt.attr('alt');
            var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-\/]))?/;
            if (regex.test(_url)) {
                _alt.css({'cursor': 'pointer'});
                _alt.unbind().bind('click', function () {
                    window.open(_url, "_blank");
                });
            }
        });
    };
    var clientCommendation = function () {
        j(".client-comments").owlCarousel({
            items: 1,
            lazyContent: true,
            afterInit: function () {
                j(this).show();
            },
            loop: true,
            margin: 0,
            autoPlay: 8000,
            itemsCustom: [[0, 1], [400, 1], [700, 1], [1000, 1], [1200, 1], [1600, 1]],
            responsiveRefreshRate: true,
            responsiveClass: true
        });
    };
    var scrollToTop = function () {
        j('.bcktop').click(function (e) {
            e.preventDefault();
            j("html, body").animate({scrollTop: 0}, '2000', 'swing');
        });
    };
    var commentvalidate = function () {
        j('.commentform').validate({
            rules: {
                author: 'required',
                email: {
                    required: true,
                    email: true
                },
                comment: 'required'
            },
            showErrors: function (errorMap, errorList) {
                j.each(this.validElements(), function (index, element) {
                    var $element = j(element);
                    $element.parent().removeClass("has-error");
                    $element.data("title", "").tooltip("destroy");
                });
                j.each(errorList, function (index, error) {
                    var $element = j(error.element);
                    $element.parent().addClass("has-error");
                    $element.tooltip("destroy").data("title", error.message).tooltip();
                });
            }
        });
        _window.resize(function () {
            j('*').tooltip('hide');
        });
    };
    var masonry = function () {
        "use strict";
        var container = j('.grid-content');
        var blogItems = function () {
            container.masonry({
                isAnimated: true,
                itemSelector: '.item-grid'
            });
            _body.find('.open-window a').unbind().bind('click', function (e) {
                e.preventDefault();
                var _this = j(this);
                var width = 400;
                var height = 400;
                window.open(_this.attr('href'), 'share', "width=" + width + ", height=" + height + ", top=" + ((screen.height) / 2 - (height / 2)) + ", left=" + (screen.width / 2 - (width / 2)));
            });
        };

        container.find('img').load(function () {
            j(this).addClass('image-loaded');
            blogItems();
        }).each(function () {
            if (this.complete) {
                j(this).load();
            }
        });
        blogItems();
        container.infinitescroll({
            "nextSelector": ".navigation .pull-right a",
            "navSelector": ".navigation",
            "itemSelector": ".item-grid",
            errorCallback: function () {
                j('#infscr-loading').animate({
                    opacity: 0.8
                }, 2000).fadeOut('normal');
            }
        }, function (newElements) {
            var $newElems = j(newElements);
            $newElems.hide();
            $newElems.imagesLoaded(function () {
                $newElems.fadeIn(200);
                container.masonry('appended', $newElems, true);
                $newElems.find('img').addClass('image-loaded');
                blogItems();
            });
        });
    };
    return{
        init: init,
        photo:photogallery
    };
})(jQuery);
(function () {
    CORE.code.init();
})(jQuery);


