<?php
include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
if (is_plugin_active('js_composer/js_composer.php')) {
    require_once(dirname(__FILE__) . '/includes/shortcodes.php');
}

require_once dirname(__FILE__) . '/install/install.php';
require_once(dirname(__FILE__) . '/includes/walker.php');
require_once dirname(__FILE__) . '/includes/pricing_table.php';
require_once dirname(__FILE__) . '/includes/client-logo.php';
require_once dirname(__FILE__) . '/includes/testimonials.php';
require_once dirname(__FILE__) . '/includes/client.php';
require_once dirname(__FILE__) . '/includes/services.php';
require_once dirname(__FILE__) . '/options-framework/options-framework.php';
require_once dirname(__FILE__) . '/includes/social_widgets.php';
require_once dirname(__FILE__) . '/includes/recent_post_widgets.php';
require_once dirname(__FILE__) . '/includes/single_event_banner.php';
require_once dirname(__FILE__) . '/includes/job.php';
require_once dirname(__FILE__) . '/includes/panel.php';
require_once dirname(__FILE__) . '/includes/shortcode-widget.php';
require_once dirname(__FILE__) . '/includes/fb_widget.php';
require_once dirname(__FILE__) . '/includes/recent-post.php';
require_once dirname(__FILE__) . '/includes/photo-gallery.php';

function parameter_theme_scripts() {
    wp_enqueue_style('font-awesome', get_template_directory_uri() . '/css/font-awesome.min.css');
    wp_enqueue_style('bootstrap', get_template_directory_uri() . '/css/bootstrap.min.css');
    wp_enqueue_style('bootstrap-theme', get_template_directory_uri() . '/css/bootstrap-theme.min.css');
    wp_enqueue_style('owl-carousel', get_template_directory_uri() . '/css/owl.carousel.css');
    wp_enqueue_style('owl-theme', get_template_directory_uri() . '/css/owl.theme.css');
    wp_enqueue_style('owl-transitions', get_template_directory_uri() . '/css/owl.transitions.css');
    wp_enqueue_style('social-media', get_template_directory_uri() . '/css/bootstrap-social.css');
    wp_enqueue_style('swipeBox', get_template_directory_uri() . '/css/swipebox.min.css');
    wp_enqueue_style('layout', get_template_directory_uri() . '/css/style.css');
    wp_enqueue_style('style', get_template_directory_uri() . '/style.css');

    wp_enqueue_script('jQuery', get_template_directory_uri() . '/js/jquery-1.12.0.min.js', array('jquery'), '1.2', true);
    
    wp_enqueue_script('validate', get_template_directory_uri() . '/js/jquery.validate.min.js', array('jquery'), '1.2', true);
    wp_enqueue_script('stellar', get_template_directory_uri() . '/js/jquery.stellar.min.js', array('jquery'), '1.2', true);
    wp_enqueue_script('swipe', get_template_directory_uri() . '/js/jquery.swipebox.min.js', array('jquery'), '1.2', true);
    wp_enqueue_script('scroll', get_template_directory_uri() . '/js/scrollSpeed.js', array('jquery'), rand(100, 1), true);
    wp_enqueue_script('easing', get_template_directory_uri() . '/js/jquery.easing.js', array('jquery'), '1.2', true);
    wp_enqueue_script('bootsjs', get_template_directory_uri() . '/js/bootstrap.min.js', array('jquery'), '1.2', true);
    wp_enqueue_script('owl', get_template_directory_uri() . '/js/owl.carousel.min.js', array('jquery'), '1.2', true);
    wp_enqueue_script('mansory', get_template_directory_uri() . '/js/masonry.js', array('jquery'), '1.2', true);
    wp_enqueue_script('slimscroll', get_template_directory_uri() . '/js/slimscroll.min.js', array('jquery'), '1.2', true);
    wp_enqueue_script('imagesloaded', get_template_directory_uri() . '/js/imagesloaded.min.js', array('jquery'), '1.2', true);
    wp_enqueue_script('infinite', get_template_directory_uri() . '/js/jquery.infinitescroll.js', array('jquery'), '1.2', true);
    wp_enqueue_script('elips', get_template_directory_uri() . '/js/elips.js', array('jquery'), '1.2', true);
    wp_enqueue_script('script', get_template_directory_uri() . '/js/script.js', array('jquery'), '1.2', true);
}

add_action('wp_enqueue_scripts', 'parameter_theme_scripts');



register_nav_menus(array(
    'primary' => __('Primary Menu', '_wp_theme'),
));

function siderbar_widgets_init() {
    register_sidebar(array(
        'name' => __('Sidebar'),
        'before_widget' => '<li class="panel panel-default %2$s">',
        'after_widget' => '</li>',
        'before_title' => '<div class="panel-heading">',
        'after_title' => '</div>',
    ));
    register_sidebar(array(
        'name' => __('Footer-One'),
        'before_widget' => '<div class="footer-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="footer-header">',
        'after_title' => '</div>',
    ));
    register_sidebar(array(
        'name' => __('Footer-Two'),
        'before_widget' => '<div class="footer-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="footer-header">',
        'after_title' => '</div>',
    ));
    register_sidebar(array(
        'name' => __('Footer-Three'),
        'before_widget' => '<div class="footer-widget %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="footer-header">',
        'after_title' => '</div>',
    ));
    register_sidebar(array(
        'name' => __('Footer-Four'),
        'before_widget' => '<div class="footer-widget  %2$s">',
        'after_widget' => '</div>',
        'before_title' => '<div class="footer-header">',
        'after_title' => '</div>',
    ));
}

add_action('widgets_init', 'siderbar_widgets_init');

function limit_text($text, $limit) {
    $text = preg_replace('|\[(.+?)\](.+?\[/\\1\])?|s', '', $text);
    $text = strip_tags($text, $allowed_tags);
    if (str_word_count($text, 0) > $limit) {
        $words = str_word_count($text, 2);
        $pos = array_keys($words);
        $text = substr($text, 0, $pos[$limit]) . '...';
    }
    return $text;
}

function register_my_menus() {
    register_nav_menus(
            array(
                'side_menu' => __('Side Menu'),
            )
    );
}

add_action('init', 'register_my_menus');

add_filter('get_avatar', 'replace_photo_class');

function replace_photo_class($avatar) {
    return str_replace(' photo', ' img-thumbnail', $avatar);
}

add_filter('widget_tag_cloud_args', 'limit_tag_in_tag_cloud_widget');
 
function limit_tag_in_tag_cloud_widget($args){
	if(isset($args['taxonomy']) && $args['taxonomy'] == 'post_tag'){
		$args['number'] = 20; //Limit number of tags
	}
	return $args;
}


function downloader($download) {
    header('Content-Type: application/octet-stream');
    header("Content-Transfer-Encoding: Binary");
    header("Content-disposition: attachment; filename='" . basename($download) . "' ");
    readfile($download);
    exit;
}
if (isset($_GET['download']) and ( $_GET['download'] != NULL)) {
    downloader($_GET['download']);
}
add_filter('single_template', function($original){
  global $post;
  $post_name = $post->post_name;
  $post_type = $post->post_type;
  $base_name = 'single-' . $post_type . '-' . $post_name . '.php';
  $template = locate_template($base_name);
  if ($template && ! empty($template)) return $template;
  return $original;
});