<?php get_header(); ?>
<div class="container ">
    <img src="<?php echo get_template_directory_uri() ?>/images/404.png" style="display: block; margin: 0 auto"/>
    <h1 class="text-center">
        PAGE NOT FOUND!
    </h1>
    <h3 class="text-center">
        Sorry, the page you requested may have been moved or deleted
    </h3>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <form role="search" class="widget-search" method="get"  action="<?php echo esc_url(home_url('/')); ?>">
                <div class="input-group">
                    <input type="text"  value="<?php echo get_search_query(); ?>" name="s" class="form-control"  placeholder="Search">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form> 
        </div>
        <div class="col-md-3"></div>
    </div>

    <div class="text-center" style="margin-top: 15px;margin-bottom: 15px;">
        <a href="<?php echo home_url(); ?>" class="btn btn-default">GET ME BACK TO HOME PAGE</a>
    </div> 
</div> 
<?php
get_footer();
