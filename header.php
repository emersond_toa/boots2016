<!DOCTYPE html>
<html lang="en">
    <head>
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <link rel="shortcut icon" href="<?php echo of_get_option('favicon'); ?>"/>
        <title><?php wp_title('|', true, 'right'); ?></title>
        <?php wp_head(); ?>
    </head>
    <?php
    if (of_get_option('enable_header') == 1) {
        $nav = 'navbar-fixed-top';
        $body = 'fixed-top';
    } else {
        $nav = '';
        $body = '';
    }
    if (of_get_option('top_bg') != '') {
        $top_nav = ' style="background-color:' . of_get_option('top_bg') . '"';
        $readmore = ' read-more';
        ?>
        <style>
            .navbar-default .navbar-nav>li>a:focus, 
            .navbar-default .navbar-nav>li>a:hover{
                opacity:0.9;
            }
            .navbar-default .navbar-nav>.open>a,
            .navbar-default .navbar-nav>.open>a:focus,
            .navbar-default .navbar-nav>.open>a:hover,
            .dropdown-menu>.active>a,
            .dropdown-menu>.active>a:focus, 
            .dropdown-menu>.active>a:hover,
            .navbar-default .navbar-nav>li>a:focus, 
            .navbar-default .navbar-nav>li>a:hover,
            .navbar-default .navbar-nav>.active>a, 
            .navbar-default .navbar-nav>.active>a:focus, 
            .navbar-default .navbar-nav>.active>a:hover {
                color: #fff;
                background-color: <?php echo of_get_option('top_bg') ?>;
                background-image: none;
            }
            .read-more{
                background-color: <?php echo of_get_option('top_bg') ?>;
                color:#FFF !important;
                text-transform: uppercase;
            }
            .read-more:hover{opacity:0.9;}
        </style> 
        <?php
    } else {
        $top_nav = '';
    }
    ?>
    <body class="<?php echo $body; ?>"> 
      
      <div class="drawer-content">
          <div class="drawer-scroll">
                <div class="search">
                    <form method="get" action="<?php echo home_url() ?>">
                        <input type="text" name="s" class="search-query" placeholder="Search here..." value="">
                        <button class="search-icon" type="submit"><a href="#"><i class="fa fa-search"></i></a></button>
                    </form>
                </div>
              <?php
              wp_nav_menu(
                      array(
                          'theme_location' => 'side_menu',
                          'menu' => 'side_menu',
                          'container' => '',
                          'container_class' => false,
                          'menu_class' => 'sidemenu',
                          'fallback_cb' => 'false'
              ));
              ?> 
          </div>  
      </div>     
        
     

        <div class="sliding-drawer">
            <nav class="primary-nav navbar navbar-default <?php echo $nav; ?>" role="navigation">

                <div class="top-nav"<?php echo $top_nav ?>>
                    <div class="container">
                        <div class="row">
                            <div class="col-sm-6 top-contact">
                                <?php if (of_get_option('phone_num') != '') { ?>
                                    <div class="top-phone"><i class="fa fa-phone"></i> <?php echo of_get_option('phone_num'); ?> </div>
                                <?php } ?>
                                <?php if (of_get_option('con_email') != '') { ?>
                                    <div class="top-email"><i class="fa fa-envelope"></i> <a style="color:#FFF" href="mailto:<?php echo of_get_option('con_email') ?>"><?php echo of_get_option('con_email') ?></a></div>
                                <?php } ?>
                            </div>
                            <div class="col-sm-6 top-social">
                                <div class="social-media">
                                    <ul>
                                        <?php if ((of_get_option('facebook_url')) != '') { ?>
                                        <li><a href="<?php echo of_get_option('facebook_url') ?>" target="_blank"><i class="fa fa-facebook"></i></a></li>
                                        <?php } ?>
                                        <?php if ((of_get_option('instagram_url')) != '') { ?>
                                            <li><a href="<?php echo of_get_option('instagram_url') ?>" target="_blank"><i class="fa fa-instagram"></i></a></li>
                                        <?php } ?>
                                        <?php if ((of_get_option('twitter_url')) != '') { ?>
                                            <li><a href="<?php echo of_get_option('twitter_url') ?>" target="_blank"><i class="fa fa-twitter"></i></a></li>
                                        <?php } ?>
                                        <?php if ((of_get_option('google_plus_url')) != '') { ?>
                                            <li><a href="<?php echo of_get_option('google_plus_url') ?>" target="_blank"><i class="fa fa-google-plus"></i></a></li>
                                        <?php } ?>
                                        <?php if ((of_get_option('youtube_url')) != '') { ?>
                                            <li><a href="<?php echo of_get_option('youtube_url') ?>" target="_blank"><i class="fa fa-youtube"></i></a></li>
                                        <?php } ?>
                                        <?php if ((of_get_option('linkedin_url')) != '') { ?>
                                            <li><a href="<?php echo of_get_option('linkedin_url') ?>" target="_blank"><i class="fa fa-linkedin"></i></a></li>
                                        <?php } ?>
                                        <?php if ((of_get_option('pinterest_url')) != '') { ?>
                                            <li><a href="<?php echo of_get_option('pinterest_url') ?>" target="_blank"><i class="fa fa-pinterest"></i></a></li>
                                        <?php } ?>
                                            <li><a class="search-bar" target="_blank"><i class="fa fa-search"></i></a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="container">

                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" >
                            <span class="sr-only">Toggle navigation</span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                        </button>
                        <a class="logo" href="<?php echo home_url(); ?>">
                            <?php if (of_get_option('logo') == '') { ?>
                                <img src="<?php echo get_template_directory_uri () ?>/images/gradient.png"/>
                            <?php } else { ?>
                                <img src="<?php echo of_get_option('logo') ?>"/>
                            <?php } ?>
                        </a>
                    </div>

                    <?php
                    wp_nav_menu(array(
                        'menu' => 'primary',
                        'theme_location' => 'primary',
                        'depth' => 2,
                        'container' => 'div',
                        'container_class' => 'collapse navbar-collapse',
                        'container_id' => 'nav',
                        'menu_class' => 'nav navbar-nav navbar-right',
                        'fallback_cb' => 'wp_bootstrap_navwalker::fallback',
                        'walker' => new wp_bootstrap_navwalker())
                    );
                    ?>
                </div>
            </nav>

