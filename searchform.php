<form role="search" class="widget-search" method="get"  action="<?php echo esc_url(home_url('/')); ?>">
    <div class="input-group">
        <input type="text"  value="<?php echo get_search_query(); ?>" name="s" class="form-control"  placeholder="Search">
        <span class="input-group-btn">
            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
        </span>
    </div>
</form>