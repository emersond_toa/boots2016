<?php

class recent_post_widgets extends WP_Widget {

    public function __construct() {
        $widget_ops = array(
            'classname' => 'recent_post_widget',
            'description' => 'Recent Post With Thumbnails',
        );
        parent::__construct('recent_post_widget', 'Recent Post With Thumbnails', $widget_ops);
    }

    public function widget($args, $instance) {
        echo $args['before_widget'];
        if (!empty($instance['title'])) {
            echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
        }
        $args = array( 'numberposts' => $instance['max_post'] );
        $recent_posts = wp_get_recent_posts($args);
        add_image_size( 'featured-thumb', 50, 50, true );
        echo '<ul class="recent-post-widget">';
        foreach ($recent_posts as $recent) {
            if ($recent['post_status'] == "publish") {
                if (has_post_thumbnail($recent["ID"])) {
                    echo '<li>
		<a href="' . get_permalink($recent["ID"]) . '" title="Look ' . esc_attr($recent["post_title"]) . '" ><span class="post-thumbnail">' . get_the_post_thumbnail($recent["ID"], 'thumbnail',array('class' => 'img-thumbnail')) .'</span><span class="post-title-wid"><strong>'. $recent["post_title"] .'</strong><br>Posted: '.$recent["post_date"].'<br>By '. get_the_author().'</span></a>';
                    echo '</li>';
                } else {
                    echo '<li>
		<a href="' . get_permalink($recent["ID"]) . '" title="Look ' . esc_attr($recent["post_title"]) . '" >' . $recent["post_title"] .''.$recent["post_date"]. '</a>';
                    echo '</li>';
                }
            }
        }
        echo '</ul>';

        echo $args['after_widget'];
    }

    public function form($instance) {
        $title = !empty($instance['title']) ? $instance['title'] : __('Recent Post', 'wp_theme');
        $max_post = !empty($instance['max_post']) ? $instance['max_post'] : __('10', 'wp_theme');
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>">
        </p>
        <p>
            <label for="<?php echo $this->get_field_id('max_post'); ?>"><?php _e('Max Post:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('max_post'); ?>" name="<?php echo $this->get_field_name('max_post'); ?>" type="text" value="<?php echo esc_attr($max_post); ?>">
        </p>
        <?php
    }

    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        $instance['max_post'] = (!empty($new_instance['max_post']) ) ? strip_tags($new_instance['max_post']) : '10';
        return $instance;
    }

}

add_action('widgets_init', function() {
    register_widget('recent_post_widgets');
});
