<?php

function photo_gallery($atts, $content = null) {
    extract(shortcode_atts(array(
        'gallery_title' => 'Photo Gallery',
        'gallery_day' => date('d'),
        'gallery_mo_year' => date('M, Y'),
        'gallery_caption' => 'Photo Caption',
        'photo_images' => ''
                    ), $atts));
    $photo_images = explode(",", $photo_images);
    $html = '';
    $tag = '';
    $class = 'id' . rand(1, 1000) . rand(1, 1000) . rand(1, 1000);

    $html .= '<div class="photo-gallery" >';
    
    if ($gallery_title) {
        $html .= '<h1 class="gallery-title">' . $gallery_title . '</h1>';
    }
    $html .= '<a href="#' . $class . '" id="' . $class . '">';
    $html .= '<span class="photo-date"><span class="cdate">'.$gallery_day.'</span><span class="ydate">'.$gallery_mo_year.'</span></span>';
    if (isset($photo_images[0])) {
        $html .= wp_get_attachment_image($photo_images[0], 'thumbnail');
    }
    $html .= '<span class="photo-galler-cap ellipsis">'.limit_text($gallery_caption,50).'</span>';
    $html .= '</a>';
    $html .= '<script>';
    $html .= 'var '.$class.' = [';
    
    foreach ($photo_images as $row) {
        $attachment = get_post( $row );
        $tag .= '{"thumbnail'.$cap.'":"'.wp_get_attachment_image_src($row)[0].'","large_photo":"'.wp_get_attachment_url($row).'","photo_title":"'.get_the_title( $row ).'","gallery_caption":"'.$attachment->post_excerpt.'"},';
//        if ($row != '') {
//            if($enable_box == 'true'){
//                $html .= '<a href="'.wp_get_attachment_url( $row ).'" class="swipebox">';
//            }
//            $html .= wp_get_attachment_image($row, 'medium');
//            if($enable_box == 'true'){
//                $html .= '</a>';
//            }
//        }
    }
    $html .= rtrim($tag,",");
    
    $html .= '];';
    $html .= '(function(j){ j("#' . $class . '").click(function(e){e.preventDefault();CORE.code.photo('.$class.');}) })(jQuery)';

    $html .= '</script>';
    
    $html .= '</div>';
    return $html;
}

add_shortcode('_photo_gallery', 'photo_gallery');
