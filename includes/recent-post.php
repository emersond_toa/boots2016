<?php

function _the_recent_post($atts, $content = null) {
    extract(shortcode_atts(array(
        'title' => '',
        'cols' => 4,
                    ), $atts));
    $args = array(
    'numberposts' => 4,
    'offset' => 0,
    'category' => 0,
    'orderby' => 'post_date',
    'order' => 'DESC',
    'include' => '',
    'exclude' => '',
    'meta_key' => '',
    'meta_value' =>'',
    'post_type' => 'post',
    'post_status' => '',
    'suppress_filters' => true );
    
    $recent_posts = wp_get_recent_posts($args);
    $html = '<div class="row grid-content">';
    foreach ($recent_posts as $recent) {
        if ($recent['post_status'] == "publish") {
            $html .= '<div class="col-md-6 col-sm-12 col-xs-12 item-grid">';
            $html .= '<div class="panel panel-default">';
            $html .= '<div class="panel-body recent-post-body">';
            $html .= '<div class="panel-thumbnail">';
            $html .= '<a href="' . get_permalink($recent["ID"]) . '" title="'. $recent["post_title"].'">'.get_the_post_thumbnail($recent["ID"], 'thumbnail').'</a>';  
            $html .= '</div>';
            $html .= '<div class="panel-content">';
            
            $html .= '<div class="recent-title"><a href="' . get_permalink($recent["ID"]) . '" title="'. $recent["post_title"].'">' . $recent["post_title"] . '</a></div>';
            $html .= '<div class="meta-tags-categories">';
            $html .= 'By: <a href="' . get_author_posts_url(get_the_author_meta('ID'), get_the_author_meta('user_nicename')) . '">' . get_the_author_meta('display_name') . '</a> ';

//            if ((get_the_tags($recent["ID"]))) {
//                $html .= '<i class="fa fa-tag"></i> ';
//                $tags = '';
//                foreach (get_the_tags($recent["ID"]) as $tag) {
//                    $tags .= '<a href="' . get_tag_link($tag->term_id) . '">' . $tag->name . '</a>, ';
//                }
//                $html .= rtrim($tags, ", ");
//            }
//
//            if ((get_the_category($recent["ID"]))) {
//                $html .= ' <i class="fa fa-folder"></i> ';
//                $ctgry = '';
//                foreach (get_the_category($recent["ID"]) as $category) {
//                    $ctgry .= '<a href="' . get_tag_link($category->term_id) . '">' . $category->name . '</a>, ';
//                }
//                $html .= rtrim($ctgry, ", ");
//            }
            
            
            $html .= '</div>';
            $html .= '<div class="panel-text  ellipsis">';
            $html .= limit_text($recent["post_content"], 25);
            $html .= '</div>';
            $html .= '</div>';
            
            $html .= '</div>';
            $html .= '<div style="overflow:hidden;padding:0px 15px 15px;">';
            $html .= '<div class="pull-right">';
            $html .= '<a href="' . get_permalink($recent["ID"]) . '" class="btn read-more">Read More <i class="fa fa-long-arrow-right"></i></a>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
            $html .= '</div>';
        }
    }
    $html .= '</div>';
    return $html;
}

add_shortcode('the_recent_posts', '_the_recent_post');


