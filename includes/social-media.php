<div class="div-social open-window">
    <?php if ((of_get_option('facebook_url')) != '') { ?>
        <a href="https://www.facebook.com/sharer/sharer.php?u=<?php the_permalink(); ?>" class="btn-facebook btn btn-social-icon"><i class="fa fa-facebook"></i></a>
    <?php } ?>
    <?php if ((of_get_option('twitter_url')) != '') { ?>   
        <a href="http://twitter.com/share?text=<?php the_title(); ?>&url=<?php the_permalink(); ?>" class="btn btn-social-icon btn-twitter"><i class="fa fa-twitter"></i></a>
    <?php } ?>

    <?php if ((of_get_option('google_plus_url')) != '') { ?>
        <a href="https://plus.google.com/share?url=<?php the_permalink(); ?>" class="btn btn-social-icon btn-google"><i class="fa fa-google"></i></a>
    <?php } ?>

    <?php if ((of_get_option('youtube_url')) != '') { ?>
        <a href="<?php echo of_get_option('youtube_url') ?>" class="btn btn-social-icon btn-pinterest"><i class="fa fa-youtube"></i></a>
    <?php } ?>

    <?php if ((of_get_option('linkedin_url')) != '') { ?>
        <a href="http://www.linkedin.com/shareArticle?mini=true&url=<?php the_permalink(); ?>&title=<?php the_title(); ?>&summary=<?php echo get_the_excerpt(); ?>&source=<?php the_permalink(); ?>" class="btn btn-social-icon btn-linkedin"><i class="fa fa-linkedin"></i></a>
    <?php } ?>

    <?php if ((of_get_option('pinterest_url')) != '') { ?>
        <a href="http://pinterest.com/pin/create/button/?url=<?php the_permalink(); ?>&media=<?php echo wp_get_attachment_url(get_post_thumbnail_id()); ?>&description=<?php echo get_the_excerpt(); ?>" class="btn btn-social-icon btn-pinterest"><i class="fa fa-pinterest"></i><?php echo wp_get_attachment_url($post->ID); ?></a>
        <?php } ?>
</div>