<?php

function _testimonials() {
    register_post_type('_testimonials', array(
        'labels' => array(
            'name' => __('Testimonials'),
            'singular_name' => __('Testimonial'),
            'add_new_item' => __('Add New Testimonial'),
        ),
        'show_ui' => true,
        'public' => true,
        'menu_icon' => 'dashicons-welcome-add-page',
        'hierarchical' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
            )
    );
    flush_rewrite_rules(false);
}

add_action('init', '_testimonials');

function _client_testimonial($atts, $content = null) {
    extract(shortcode_atts(array(
        'title' => '',
        'description' => 'We want our clients to be always satisfied with our work. See what they have to say about us.',
        'font_color'=> '333333'
                    ), $atts));
    $html = '<div class="comment-box container">';
    if ($title) {
        $html .= '<h2 class="happy-client-title" style="color:'.$font_color.'">' . $title . '</h2>';
    }
    $html .= '<div class="row">';
    $maxpost = 1000;
    global $post;
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $args = array(
        'post_type' => '_testimonials',
        'posts_per_page' => $maxpost,
        'paged' => $paged
    );
    $the_query = new WP_Query($args);
    $myposts = get_posts($args);
    foreach ($myposts as $post) :
        setup_postdata($post);
        $link = get_permalink($post->ID);
        $html .= ' <div class="col_">';
        
        $html .= ' <div class="comm-box">';
        $html .= ' <div class="comm-head">';
        $html .= ' <div class="comment-head">';
        if (get_the_post_thumbnail($post->ID)) {

            $html .= get_the_post_thumbnail($post->ID, 'thumbnail');
 
        }
        $html .= '</div>   ';
        $html .= '<div class="comm-name" style="color:'.$font_color.'">'.get_the_title().'</div>';
        $html .= '</div>   ';
        $html .= '</div>   ';

        $html .= '<div class="comm-comment" style="color:'.$font_color.'">';
        $html .= get_the_content();
        $html .= '</div>';
        $html .= '<div class="comm-footer"></div>';
        $html .= '</div>';
    endforeach;
    wp_reset_postdata();
    
    $html .= '</div>';
    
    $html .= '</div>';
    return $html;
}

add_shortcode('_testimonial', '_client_testimonial');

