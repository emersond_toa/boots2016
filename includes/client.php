<?php

function client_types() {
    register_post_type('client-comment', array(
        'labels' => array(
            'name' => __('Clients'),
            'singular_name' => __('Client'),
            'add_new_item' => __('Add New Comment'),
        ),
        'show_ui' => true,
        'public' => true,
        'menu_icon' => 'dashicons-groups',
        'hierarchical' => true,
        'supports' => array('title', 'editor', 'thumbnail'),
            )
    );
    flush_rewrite_rules(false);
}

add_action('init', 'client_types');

function _client($atts, $content = null) {
    extract(shortcode_atts(array(
        'title' => 'HAPPY CLIENTS',
        'description' => 'We want our clients to be always satisfied with our work. See what they have to say about us.',
        'font_color'=> '333333'
                    ), $atts));
    $html = '';
    if ($title) {
        $html .= '<h2 class="happy-client-title" style="color:'.$font_color.'">' . $title . '</h2>';
    }
    $html .= '<div class="client-comments">';
    $maxpost = 10;
    global $post;
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $args = array(
        'post_type' => 'client-comment',
        'posts_per_page' => $maxpost,
        'paged' => $paged
    );
    $the_query = new WP_Query($args);
    $myposts = get_posts($args);
    foreach ($myposts as $post) :
        setup_postdata($post);
        $link = get_permalink($post->ID);
        $html .= ' <div class="item">';

        $html .= ' <div class="comment-head">';
        if (get_the_post_thumbnail($post->ID)) {
            $html .= ' <a href="' . $link . '">';
            $html .= get_the_post_thumbnail($post->ID, 'thumbnail');
            $html .= '</a>   ';
        }
        $html .= '</div>   ';
        $html .= '<div class="client-name" style="color:'.$font_color.'">'.get_the_title().'</div>';
        if ($description) {
            $html .= '<h3 class="happy-client-title" style="color:'.$font_color.'">' . $description . '</h3>';
        }
        $html .= '<div class="comment-text" style="color:'.$font_color.'">';
        $html .= limit_text(get_the_content(), 50);
        $html .= '</div>';
        $html .= '<div class="read-more-btn">';
        $html .= '<a href="' . $link . '" class="btn btn-info btn-sm">Read More</a>';
        $html .= '</div>';
        $html .= '</div>';
    endforeach;
    wp_reset_postdata();
    $html .= '</div>';
    return $html;
}

add_shortcode('_client_comment', '_client');







function masonry_client($atts, $content = null) {
    extract(shortcode_atts(array(
        'title' => 'HAPPY CLIENTS',
        'description' => 'We want our clients to be always satisfied with our work. See what they have to say about us.',
        'font_color'=> '333333'
                    ), $atts));
    $html = '';
    if ($title) {
        $html .= '<h2 class="happy-client-title" style="color:'.$font_color.'">' . $title . '</h2>';
    }
    $html = '<div class="comment-box container">';
    $html .= '<div class="row">';
    $maxpost = 1000;
    global $post;
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $args = array(
        'post_type' => 'client-comment',
        'posts_per_page' => $maxpost,
        'paged' => $paged
    );
    $the_query = new WP_Query($args);
    $myposts = get_posts($args);
    foreach ($myposts as $post) :
        setup_postdata($post);
        $link = get_permalink($post->ID);
        $html .= ' <div class="col_">';
        
        $html .= ' <div class="comm-box">';
        $html .= ' <div class="comm-head">';
        $html .= ' <div class="comment-head">';
        if (get_the_post_thumbnail($post->ID)) {

            $html .= get_the_post_thumbnail($post->ID, 'thumbnail');
 
        }
        $html .= '</div>   ';
        $html .= '<div class="comm-name" style="color:'.$font_color.'">'.get_the_title().'</div>';
        $html .= '</div>   ';
        $html .= '</div>   ';

        $html .= '<div class="comm-comment" style="color:'.$font_color.'">';
        $html .= get_the_content();
        $html .= '</div>';
        $html .= '<div class="comm-footer"></div>';
        $html .= '</div>';
    endforeach;
    wp_reset_postdata();
    $html .= '</div>';
    $html .= '</div>';
    return $html;
}

add_shortcode('_masonry_client', 'masonry_client');
