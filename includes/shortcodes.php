<?php
wpb_map(array(
    "name" => __("Facebook widget"),
    "base" => "facebook_widget",
    "class" => "",
    "icon" => "vc_element-icon icon-wpb-balloon-facebook-left",
    "category" => 'Content',
    "params" => array(
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Add Title"),
            "param_name" => "add_title",
            "description" => __("Add Title")
        ),array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Facebook Page URL"),
            "param_name" => "url",
            "value"=> "https://www.facebook.com/facebook",
            "description" => __("Facebook Page URL")
        ),array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "hide",
            "heading" => __("Tabs"),
            "param_name" => "tab",
            "description" => __("Tabs"),
            "value" => __("timeline"),
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "hide",
            "heading" => __("Height", "js_composer"),
            "param_name" => "height",
            "description" => __("Height")
        ),
        array(
            "type" => "checkbox",
            "holder" => "div",
            "class" => "hide",
            "value" => "1",
            "heading" => __("Enable panel"),
            "param_name" => "panel",
            "description" => __("Enable panel", "js_composer"),
        )),
));

wpb_map(array(
    "name" => __("Single Event Calendar"),
    "base" => "single_event_banner",
    "class" => "",
    "icon" => "single-banner",
    "category" => 'Content',
    "params" => array(
        array(
            "type" => "attach_image",
            "heading" => __("Main Image", "js_composer"),
            "param_name" => "main_image",
            "value" => "",
            "description" => __("Select image from media library.", "js_composer")
        ),array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Add Title"),
            "param_name" => "add_title",
            "description" => __("Add Title")
        ),
        array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "",
            "heading" => __("Content"),
            "param_name" => "content",
            "value" => __("<p>I am test text block. Click edit button to change this text.</p>"),
            "description" => __("Enter your content.", "js_composer")
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Add URL", "js_composer"),
            "param_name" => "add_url",
            "description" => __("Add URL")
        )),
));

wpb_map(array(
    "name" => __("Photo gallery"),
    "base" => "_photo_gallery",
    "class" => "",
    "icon" => "single-banner",
    "category" => 'Content',
    "params" => array(
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Gallery Title"),
            "param_name" => "gallery_title",
            "description" => __("Add Title")
        ),
        array(
            "type" => "attach_images",
            "heading" => __("Add photo", "js_composer"),
            "param_name" => "photo_images",
            "value" => "",
            "description" => __("Select image from media library.", "js_composer")
        ),
        array(
            "type" => "textarea",
            "holder" => "div",
            "class" => "textarea",
            "heading" => __("Add Caption"),
            "param_name" => "gallery_caption",
            "description" => __("Add Gallery Caption")
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "textfield",
            "heading" => __("Day"),
            "param_name" => "gallery_day",
            "value" => date('d'),
            "description" => __("Add date")
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "textfield",
            "heading" => __("Month and Year"),
            "param_name" => "gallery_mo_year",
            "value" => date('M,Y'),
            "description" => __("Add date")
        )),
));



wpb_map(array(
    "name" => __("Job Opening"),
    "base" => "jobopening",
    "class" => "",
    "icon" => "single-banner",
    "category" => 'Content',
    "params" => array(
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Add Title"),
            "param_name" => "add_title",
            "description" => __("Add Title")
        ),
        array(
            "type" => "colorpicker",
            "holder" => "div",
            "class" => "",
            "heading" => __("Font color"),
            "param_name" => "font_color",
            "description" => __("Select font color.", "js_composer")
        )),
));





wpb_map(array(
    "name" => __("Panel Widget"),
    "base" => "panel_widget",
    "class" => "",
    "icon" => "single-banner",
    "category" => 'Content',
    "params" => array(
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Add Title"),
            "param_name" => "add_title",
            "description" => __("Add Title")
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Add Shorcode"),
            "param_name" => "add_shortcode",
            "description" => __("Enter your shortcode.", "js_composer")
        ),
        array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "",
            "heading" => __("Add content"),
            "param_name" => "content",
            "description" => __("Add content", "js_composer"),
        )),
));

wpb_map(array(
    "name" => __("Image Banner"),
    "base" => "image_banner_base",
    "class" => "",
    "icon" => "single-banner",
    "category" => 'Content',
    "params" => array(
        array(
            "type" => "attach_image",
            "heading" => __("Main Image", "js_composer"),
            "param_name" => "main_image",
            "value" => "",
            "description" => __("Select image from media library.", "js_composer")
        ),
        array(
            "type" => "attach_image",
            "heading" => __("Attach Logo", "js_composer"),
            "param_name" => "attach_logo",
            "value" => "",
            "description" => __("Select image from media library.", "js_composer")
        ),
        array(
            "type" => "colorpicker",
            "heading" => __("Select Description Background Color", "js_composer"),
            "param_name" => "background",
            "value" => "",
            "description" => __("Select Description Background Color.", "js_composer")
        ),
        array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "",
            "heading" => __("Content"),
            "param_name" => "content",
            "value" => __("<p>I am test text block. Click edit button to change this text.</p>"),
            "description" => __("Enter your content.", "js_composer")
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Max Height", "js_composer"),
            "param_name" => "max_height",
            "description" => __("Description Max height")
        )),
));
wpb_map(array(
    "name" => __("Our Client"),
    "base" => "_client_logo",
    "class" => "",
    "icon" => "our-client",
    "category" => 'Content',
    "params" => array(
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Client title logo"),
            "param_name" => "title",
            "description" => __("Add client logo title.", "js_composer"),
        ),
        array(
            "type" => "attach_images",
            "heading" => __("Client Logos", "js_composer"),
            "param_name" => "logo_images",
            "value" => "",
            "description" => __("Select image from media library.", "js_composer")
        ),
        array(
            "type" => "checkbox",
            "holder" => "div",
            "class" => "hide",
            "value" => "1",
            "heading" => __("Enable swipebox"),
            "param_name" => "enable_box",
            "description" => __("Enable swipebox", "js_composer"),
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("logo Items"),
            "param_name" => "logo_items",
            "description" => __("Add client logo title.", "js_composer"),
        ),),
));
wpb_map(array(
    "name" => __("Services widgets"),
    "base" => "services_widget",
    "class" => "",
    "icon" => "services",
    "category" => 'Content',
    "params" => array(
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "hide",
            "heading" => __("Add Class"),
            "param_name" => "add_class",
            "description" => __("Add Class", "js_composer"),
        ),
        array(
            "type" => "attach_images",
            "heading" => __("Service image", "js_composer"),
            "param_name" => "service_img",
            "value" => "",
            "description" => __("Select image service header", "js_composer")
        ),
        array(
            "type" => "iconpicker",
            "holder" => "div",
            "class" => "",
            "heading" => __("Font icon class"),
            "param_name" => "font_icon",
            "description" => __("Add font icon class <a href='https://fortawesome.github.io/Font-Awesome/icons/' target='_blank'>See the documentation</a>.", "js_composer"),
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "heading" => __("Heading"),
            "param_name" => "heading",
            "description" => __("Add heading text.", "js_composer"),
        ),
        array(
            "type" => "textarea",
            "holder" => "div",
            "class" => "hide",
            "heading" => __("Body text"),
            "param_name" => "body_text",
            "description" => __("Add body text.", "js_composer"),
        ),
        array(
            "type" => "colorpicker",
            "class" => "hide",
            "heading" => __("Icon Background Color", "js_composer"),
            "param_name" => "icon_background",
            "value" => "",
            "description" => __("Select icon Post Background Color.", "js_composer")
        ),
        array(
            "type" => "colorpicker",
            "class" => "hide",
            "heading" => __("Text Color", "js_composer"),
            "param_name" => "font_color",
            "value" => "",
            "description" => __("Select font Color.", "js_composer")
        ), 
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "text-center",
            "heading" => __("Button Value"),
            "param_name" => "btn_value",
            "description" => __("Add button value.", "js_composer"),
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "hide",
            "heading" => __("Button link"),
            "param_name" => "btn_link",
            "description" => __("Add button link.", "js_composer"),
        ),
        array(
            "type" => "colorpicker",
            "heading" => __("Btn  Color", "js_composer"),
            "param_name" => "btn_color",
            "class" => "hide",
            "value" => "",
            "description" => __("Select button Color.", "js_composer")
        )),
));



wpb_map(array(
    "name" => __("Recent Posts"),
    "base" => "the_recent_posts",
    "class" => "",
    "icon" => "icon-recent-post",
    "category" => 'Content',
    "params" => array(
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Recent Posts title"),
            "param_name" => "title",
            "description" => __("Add Recent Posts title.", "js_composer"),
        ),
        array(
            "type" => "dropdown",
            "heading" => __("Grid elements per row", "js_composer"),
            "param_name" => "cols",
            "value" => array(3,4),
            "description" => __("Add Grid number per row.", "js_composer")
        )
    ),
));

wpb_map(array(
    "name" => __("Pricing Table"),
    "base" => "_pricing_table",
    "class" => "",
    "icon" => "pricing_table",
    "category" => 'Content',
    "params" => array(
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Pricing table title"),
            "param_name" => "title",
            "description" => __("Add pricing table title", "js_composer"),
        ), array(
            "type" => "colorpicker",
            "heading" => __("Pricing table color", "js_composer"),
            "param_name" => "table_color",
            "value" => "#FFF",
            "description" => __("Select table color", "js_composer")
        ), array(
            "type" => "colorpicker",
            "heading" => __("Pricing text color", "js_composer"),
            "param_name" => "text_color",
            "value" => "#FFFFFF",
            "description" => __("Select text color", "js_composer")
        ), array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Price"),
            "param_name" => "table_price",
            "description" => __("Add table price", "js_composer"),
        ), array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Per Price"),
            "param_name" => "per_price",
            "value" => "PER MONTH",
            "description" => __("Add per month, year", "js_composer"),
        ),
        array(
            "type" => "textarea_html",
            "holder" => "div",
            "class" => "",
            "heading" => __("List of the items"),
            "param_name" => "content",
            "description" => __("Add the List of the items.", "js_composer"),
        ),
        array(
            "type" => "checkbox",
            "holder" => "div",
            "class" => "hide",
            "value" => "1",
            "heading" => __("Add button"),
            "param_name" => "enable_button",
            "description" => __("Enable button", "js_composer"),
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Button value"),
            "param_name" => "btn_value",
            "value" => "SIGN UP",
            "description" => __("Add button value.", "js_composer"),
        ),
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Button link"),
            "param_name" => "link",
            "value" => "http://",
            "description" => __("Add button link.", "js_composer"),
        ),
    )
));

wpb_map(array(
    "name" => __("Client Comments"),
    "base" => "_client_comment",
    "class" => "",
    "icon" => "icon-client-comments",
    "category" => 'Content',
    'admin_enqueue_css' => array(get_template_directory_uri() . '/css/vc_extend.css'),
    "params" => array(
        array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Happy client title"),
            "param_name" => "title",
            "value" => __("HAPPY CLIENTS"),
            "description" => __("Add Happy client title.", "js_composer")
        ), array(
            "type" => "colorpicker",
            "heading" => __("Client comment font color", "js_composer"),
            "param_name" => "font_color",
            "value" => "",
            "description" => __("Select comment font color.", "js_composer")
        ), array(
            "type" => "textfield",
            "holder" => "div",
            "class" => "",
            "heading" => __("Description"),
            "param_name" => "description",
            "value" => __("We want our clients to be always satisfied with our work. See what they have to say about us."),
            "description" => __("Description", "js_composer")
        ))
));

function overwrite_shortcode() {
    $setting_row = array(
        "params" => array(
            array(
                "type" => "dropdown",
                "heading" => __("Row Style", "js_composer"),
                "param_name" => "row_style",
                "value" => array(__("None", "js_composer") => "nostyle", __("Custom Color", "js_composer") => "bg-row", __("Image", "js_composer") => "image", __("Parallax", "js_composer") => "parallax", __("Slider", "js_composer") => "slider", __("Video", "js_composer") => "video"),
                "description" => __("You can choose background color, image or parallax image for your row, choose slider if you want to add Home Page Layer Slider in your row", "js_composer")
            ),
            array(
                "type" => "attach_image",
                "heading" => __("Video Image", "js_composer"),
                "param_name" => "video_img",
                "value" => "",
                "description" => __("Select image for non-support video background", "js_composer"),
                "dependency" => Array('element' => "row_style", 'value' => array('video'))
            ),
            array(
                "type" => "textfield",
                "heading" => __("Video MP4 URL", "js_composer"),
                "param_name" => "video_url",
                "description" => __("Add your video url, mp4 file type.", "js_composer"),
                "dependency" => Array('element' => "row_style", 'value' => array('video'))
            ),
            array(
                "type" => "textfield",
                "heading" => __("Video .webm URL", "js_composer"),
                "param_name" => "video_url_webm",
                "description" => __("Add your video url, webm file type.", "js_composer"),
                "dependency" => Array('element' => "row_style", 'value' => array('video'))
            ),
            array(
                "type" => "textfield",
                "heading" => __("Video .ogv URL", "js_composer"),
                "param_name" => "video_url_ogv",
                "description" => __("Add your video url, webm file type.", "js_composer"),
                "dependency" => Array('element' => "row_style", 'value' => array('video'))
            ),
            array(
                "type" => "colorpicker",
                "heading" => __("Row Background Color", "js_composer"),
                "param_name" => "custombgcolor",
                "description" => __("Select custom background color for rows.", "js_composer"),
                "dependency" => Array('element' => "row_style", 'value' => array('bg-row'))
            ),
            array(
                "type" => 'checkbox',
                "heading" => __("Remove Padding Your Container", "js_composer"),
                "param_name" => "removepadding",
                "description" => __("If selected, your container have not space top and bottom", "js_composer"),
                "value" => Array(__("Yes, please", "js_composer") => 'yes'),
                "dependency" => Array('element' => "row_style", 'value' => array('bg-row'))
            ),
            array(
                "type" => "attach_image",
                "heading" => __("Image", "js_composer"),
                "param_name" => "custom_image",
                "value" => "",
                "description" => __("Select image from media library.", "js_composer"),
                "dependency" => Array('element' => "row_style", 'value' => array('image', 'parallax'))
            ),
            array(
                "type" => "textfield",
                "heading" => __("Parallax Image Ratio", "js_composer"),
                "param_name" => "image_ratio",
                "description" => __("You need set your parallax effects ratio, please look documentation", "js_composer"),
                "dependency" => Array('element' => "row_style", 'value' => array('parallax'))
            ),
            array(
                "type" => 'checkbox',
                "heading" => __("Parallax pattern", "js_composer"),
                "param_name" => "pattern_parallax",
                "value" => Array(__("Yes, please", "js_composer") => 'yes'),
                "description" => __("Add Parallax pattern", "js_composer"),
                "dependency" => Array('element' => "row_style", 'value' => array('parallax'))
            ),
            array(
                "type" => 'checkbox',
                "heading" => __("Add Padding to Container", "js_composer"),
                "param_name" => "addpadding",
                "description" => __("If selected, your content will have space top and bottom", "js_composer"),
                "value" => Array(__("Yes, please", "js_composer") => 'yes'),
                "dependency" => Array('element' => "row_style", 'value' => array('parallax'))
            ),
            array(
                "type" => "dropdown",
                "heading" => __("Background Repeat", "js_composer"),
                "param_name" => "image_repeat",
                "value" => array(__("No Repeat", "js_composer") => "no-repeat", __("Repeat X", "js_composer") => "repeat-x", __("Repeat Y", "js_composer") => "repeat-y", __("Repeat", "js_composer") => "repeat"),
                "description" => __("Choose repeat of your image", "js_composer"),
                "dependency" => Array('element' => "row_style", 'value' => array('image'))
            ),
            array(
                "type" => 'checkbox',
                "heading" => __("Background Size", "js_composer"),
                "param_name" => "b_size",
                "description" => __("If selected, your image has full-width", "js_composer"),
                "value" => Array(__("Yes, please", "js_composer") => 'yes'),
                "dependency" => Array('element' => "row_style", 'value' => array('image', 'parallax'))
            ),
            array(
                "type" => "textfield",
                "heading" => __("Extra class name", "js_composer"),
                "param_name" => "elm_class",
                "description" => __("If you wish to style particular content element differently, then use this field to add a class name and then refer to it in your css file.", "js_composer")
            )
        )
    );
    vc_map_update('vc_row', $setting_row);
}

add_action('wp_loaded', 'overwrite_shortcode');
