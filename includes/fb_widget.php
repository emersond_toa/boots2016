<?php

function facebook_widgets($atts, $content = null) {
    extract(shortcode_atts(array(
        'add_title' => 'Like Us',
        'url' => 'https://www.facebook.com/facebook',
        'tab' => 'timeline',
        'height' => '300',
        'panel' => '0',
        
                    ), $atts));
     $html = ''; 
    if($panel == true){
       $html .= '<div class="panel panel-default job-panel"> <div class="panel-heading" style="color:333333;font-weight:bold;font-size:16px;">'.$add_title.'</div><div class="panel-body">'; 
    }
    $html .= '<div class="fb-page" data-href="'.$url.'" data-tabs="'.$tab.'" data-height="'.$height.'" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">';
    $html .= '<div class="fb-xfbml-parse-ignore">';
    $html .= '<blockquote cite="'.$url.'">';
    $html .= '<a href="'.$url.'">Facebook</a>';
    $html .= '</blockquote>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '<div id="fb-root"></div>';
    $html .= '<script>(function(d, s, id) {';
    $html .= 'var js, fjs = d.getElementsByTagName(s)[0];';
    $html .= 'if (d.getElementById(id)) return;';
    $html .= 'js = d.createElement(s); js.id = id;';
    $html .= 'js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.6";';
    $html .= 'fjs.parentNode.insertBefore(js, fjs);';
    $html .= "}(document, 'script', 'facebook-jssdk'));</script>";
    
    if($panel){
       $html .= '</div></div>'; 
    }
    
    
    return $html;
}
add_shortcode('facebook_widget', 'facebook_widgets');
