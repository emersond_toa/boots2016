<?php

function job() {
    register_post_type('job', array(
        'labels' => array(
            'name' => __('Job Openings'),
            'singular_name' => __('Job Opening'),
            'add_new_item' => __('Add New Job Opening'),
        ),
        'show_ui' => true,
        'public' => true,
        'menu_icon' => 'dashicons-welcome-add-page',
        'hierarchical' => true,
        'supports' => array('title', 'editor', 'thumbnail',  'page-attributes'),
            )
    );
    flush_rewrite_rules(false);
}

add_action('init', 'job');

function _jobopening($atts, $content = null) {
    extract(shortcode_atts(array(
        'title' => 'CURRENT OPENINGS',
        'font_color' => '333333'
                    ), $atts));

    $html = '<div class="panel panel-default job-panel">';
    if ($title) {
        $html .= '<div class="panel-heading" style="color:' . $font_color . ';font-weight:bold;font-size:16px;">' . $title . '</div>';
    }
    $html .= '<div class="panel-body">';
    $html .= '<div class="row" >';
    $maxpost = 1000;
    global $post;
    $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
    $args = array(
        'post_type' => 'job',
        'posts_per_page' => $maxpost,
        'paged' => $paged
    );
    $the_query = new WP_Query($args);
    $myposts = get_posts($args);
    foreach ($myposts as $post) :
        setup_postdata($post);
        $link = get_permalink($post->ID);
        $html .= '<div class="col-sm-6"><a href="'.$link.'" style="color:'.$font_color.'" title="'.get_the_title().'" class="job-list btn btn-default btn-lg btn-block"><span><i class="fa fa-arrow-circle-right"></i> '.get_the_title().'</span></a></div>';
        //$html .= '<div class="col-sm-6"><a href="'.$link.'" style="color:'.$font_color.'" class="job-list btn btn-default btn-lg btn-block"><span><i class="fa fa-arrow-circle-right"></i> '.get_the_title().'</span>Posted Date: '.get_the_date().'</a></div>';
        endforeach;
    wp_reset_postdata();
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    return $html;
}
add_shortcode('jobopening', '_jobopening');
class job_opening extends WP_Widget {
    public function __construct() {
        $widget_ops = array(
            'classname' => 'job_opening',
            'description' => 'Job Opening',
        );
        parent::__construct('job_opening', 'Job Opening', $widget_ops);
    }
    public function widget($args, $instance) {
        echo $args['before_widget'];
        if (!empty($instance['title'])) {
            echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
        }
        $maxpost = 1000;
        global $post;
        $paged = ( get_query_var('paged') ) ? get_query_var('paged') : 1;
        $args = array(
            'post_type' => 'job',
            'posts_per_page' => $maxpost,
            'paged' => $paged
        );
        $html = '<ul>';
        $the_query = new WP_Query($args);
        $myposts = get_posts($args);
        foreach ($myposts as $post) :
            setup_postdata($post);
            $link = get_permalink($post->ID);
            $html .= '<li><a href="' . $link . '">' . get_the_title() . '</a></li>';
        endforeach;
        echo $html .= '</ul>';
        wp_reset_postdata();
        echo $args['after_widget'];
    }
    public function form($instance) {
        $title = !empty($instance['title']) ? $instance['title'] : __('CURRENT OPENINGS', '_wp');
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>">
        </p>
        <?php
    }
    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }
}
add_action('widgets_init', function() {
    register_widget('job_opening');
});
