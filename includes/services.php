<?php

function services_widgets($atts, $content = null) {
    extract(shortcode_atts(array(
        'add_class' => '',
        'font_icon' => '',
        'service_img' => '',
        'effect' => 'fade',
        'heading' => 'Service',
        'body_text' => '',
        'icon_background' => '#333',
        'font_color' => '#FFF',
        'btn_value' => '',
        'btn_link' => 'http://',
        'btn_color' => '#333',
                    ), $atts));
    $service_img = explode(",", $service_img);
    $html = '<div class="services-widget ' . $add_class . '">';
    if ($font_icon) {
        $html .= '<div class="service-icon ' . $font_icon . '" style="background:' . $icon_background . '"></div>';
    }
    if ($service_img) {
        $html .= '<div class="ed-slider">';
        $html .= '<ul class="slider'.rand(0,200).'" data-effect="'.$effect.'">';
        foreach ($service_img as $row) {
            if ($row != '') {
                $html .= '<li>'.wp_get_attachment_image($row, 'large').'</li>';
            }
        }
        $html .= '</ul>';
        $html .= '</div>';
    }
    $html .= '<div class="service-title">' . $heading . '</div>';
    if ($body_text) {
        $html .= '<div class="service-body">' . $body_text . '</div>';
    }
    if ($btn_value) {
        $html .= '<div class="service-button">';
        $html .= '<a href="' . $btn_link . '" class="btn" style="color:#fff;background:' . $btn_color . '">' . $btn_value . '</a>';
        $html .= '</div>';
    }
    $html .= '</div>';
    return $html;
}

add_shortcode('services_widget', 'services_widgets');
