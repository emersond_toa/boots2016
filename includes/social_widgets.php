<?php

class social_widget extends WP_Widget {

    public function __construct() {
        $widget_ops = array(
            'classname' => 'social_widget',
            'description' => 'Social Buttons',
        );
        parent::__construct('social_widget', 'Social Buttons', $widget_ops);
    }

    public function widget($args, $instance) {

        echo $args['before_widget'];
        if (!empty($instance['title'])) {
            echo $args['before_title'] . apply_filters('widget_title', $instance['title']) . $args['after_title'];
        }
        ?>
        <div class="social-media">
            <?php if ((of_get_option('facebook_url')) != '') { ?>
                <a href="<?php echo of_get_option('facebook_url') ?>" class="btn-facebook btn" target="_blank"><i class="fa fa-facebook"></i></a>
            <?php } ?>
            <?php if ((of_get_option('instagram_url')) != '') { ?>
                <a href="<?php echo of_get_option('instagram_url') ?>" class="btn-instagram btn" target="_blank"><i class="fa fa-instagram"></i></a>
            <?php } ?>
            <?php if ((of_get_option('twitter_url')) != '') { ?>
                <a href="<?php echo of_get_option('twitter_url') ?>" class="btn-twitter btn" target="_blank"><i class="fa fa-twitter"></i></a>
            <?php } ?>
            <?php if ((of_get_option('google_plus_url')) != '') { ?>
                <a href="<?php echo of_get_option('google_plus_url') ?>" class="btn-google btn" target="_blank"><i class="fa fa-google-plus"></i></a>
            <?php } ?>
            <?php if ((of_get_option('youtube_url')) != '') { ?>
                <a href="<?php echo of_get_option('youtube_url') ?>" class="btn-pinterest btn" target="_blank"><i class="fa fa-youtube"></i></a>
            <?php } ?>
            <?php if ((of_get_option('linkedin_url')) != '') { ?>
                <a href="<?php echo of_get_option('pinterest_url') ?>" class="btn-linkedin btn" target="_blank"><i class="fa fa-linkedin"></i></a>
            <?php } ?>
            <?php if ((of_get_option('pinterest_url')) != '') { ?>
                <a href="<?php echo of_get_option('pinterest_url') ?>" class="btn-pinterest btn" target="_blank"><i class="fa fa-pinterest"></i></a>
        <?php } ?>
        </div>
        <?php
        echo $args['after_widget'];
    }

    public function form($instance) {
        $title = !empty($instance['title']) ? $instance['title'] : __('Share Us', 'text_domain');
        ?>
        <p>
            <label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label> 
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>">
        </p>
        <?php
    }

    public function update($new_instance, $old_instance) {
        $instance = array();
        $instance['title'] = (!empty($new_instance['title']) ) ? strip_tags($new_instance['title']) : '';
        return $instance;
    }

}

add_action('widgets_init', function() {
    register_widget('social_widget');
});
