<?php

function panel_widgets($atts, $content = null) {
    extract(shortcode_atts(array(
        'add_title' => '',
        'add_shortcode' => '',
                    ), $atts));
    $orig = array('[', ']', '"', '`', '{', '}');
    $repl = array('', '', "'", '', '', '');
    $result = str_replace($orig, $repl, $add_shortcode);
    $html = '<div class="panel panel-default widget-panel">';
    $html .= '<div class="panel-heading" style="color:333333;font-weight:bold;font-size:16px;">' . $add_title . '</div>';

    $html .= '<div class="panel-body">';
    if ($add_shortcode) {
        do_shortcode('[' . $result . ']');
    }
    if ($content) {
       $html .= $content;
    }
    
    $html .= '</div>';


    $html .= '</div>';
    return $html;
}

add_shortcode('panel_widget', 'panel_widgets');
