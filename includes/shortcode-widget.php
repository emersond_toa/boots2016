<?php
define('SHORTCODE_WIDGET_WP', 'shortcode-widget');
class Shortcode_Widget extends WP_Widget {
    public function __construct() {
        $widget_ops = array('classname' => 'shortcode_widget', 'description' => __('Shortcode or HTML or Plain Text.', SHORTCODE_WIDGET_WP));
        $control_ops = array('width' => 400, 'height' => 350);
        parent::__construct('shortcode-widget', __('Shortcode Widget', SHORTCODE_WIDGET_WP), $widget_ops, $control_ops);
    }
    public function widget($args, $instance) {
        /** This filter is documented in wp-includes/default-widgets.php */
        $title = apply_filters('widget_title', empty($instance['title']) ? '' : $instance['title'], $instance, $this->id_base);

        $text = do_shortcode(apply_filters('widget_text', empty($instance['text']) ? '' : $instance['text'], $instance));
        echo $args['before_widget'];
        if (!empty($title)) {
            echo $args['before_title'] . $title . $args['after_title'];
        }
        ?>
        <div class="panel-body"><?php echo!empty($instance['filter']) ? wpautop($text) : $text; ?></div>
        <?php
        echo $args['after_widget'];
    }
    public function update($new_instance, $old_instance) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        if (current_user_can('unfiltered_html'))
            $instance['text'] = $new_instance['text'];
        else
            $instance['text'] = stripslashes(wp_filter_post_kses(addslashes($new_instance['text']))); // wp_filter_post_kses() expects slashed
        $instance['filter'] = !empty($new_instance['filter']);
        return $instance;
    }
    public function form($instance) {
        $instance = wp_parse_args((array) $instance, array('title' => '', 'text' => ''));
        $title = strip_tags($instance['title']);
        $text = esc_textarea($instance['text']);
        ?>
        <p><label for="<?php echo $this->get_field_id('title'); ?>"><?php _e('Title:', SHORTCODE_WIDGET_WP); ?></label>
            <input class="widefat" id="<?php echo $this->get_field_id('title'); ?>" name="<?php echo $this->get_field_name('title'); ?>" type="text" value="<?php echo esc_attr($title); ?>" /></p>

        <p><label for="<?php echo $this->get_field_id('text'); ?>"><?php _e('Content:'); ?></label>
            <textarea class="widefat" rows="16" cols="20" id="<?php echo $this->get_field_id('text'); ?>" name="<?php echo $this->get_field_name('text'); ?>"><?php echo $text; ?></textarea></p>

        <p><input id="<?php echo $this->get_field_id('filter'); ?>" name="<?php echo $this->get_field_name('filter'); ?>" type="checkbox" <?php checked(isset($instance['filter']) ? $instance['filter'] : 0); ?> />&nbsp;<label for="<?php echo $this->get_field_id('filter'); ?>"><?php _e('Automatically add paragraphs', SHORTCODE_WIDGET_WP); ?></label></p>
        <?php
    }

}
function shortcode_widget_init() {
    register_widget('Shortcode_Widget');
}
add_action('widgets_init', 'shortcode_widget_init');

function shortcode_widget_load_text_domain() {
    load_plugin_textdomain(SHORTCODE_WIDGET_WP, false, dirname(plugin_basename(__FILE__)) . '/languages/');
}
add_action('plugins_loaded', 'shortcode_widget_load_text_domain');
add_shortcode('shortcode_widget_test', 'shortcode_widget_test_output');
function shortcode_widget_test_output($args) {
    return __("It works", SHORTCODE_WIDGET_WP);
}
