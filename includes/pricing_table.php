<?php
function _pricing_tables($atts, $content = null) {
    extract(shortcode_atts(array(
        'title' => 'BASIC',
        'table_color' => '#FFFFFF',
        'text_color'=> '#333',
        'table_price'=> '$300',
        'per_price'=> 'PER MONTH',
        'list_item'=> 'r',
        'enable_button'=> '',
        'btn_value'=> 'SIGN UP',
        'link'=> 'http://'
                    ), $atts));
    $html = '<div class="pricing-table panel panel-default">';
    $html .= '<div class="panel-heading" style="text-shadow: 1px 1px 2px '.$table_color.';background-color:'.$table_color.';border-color:'.$table_color.';color:'.$text_color.'">'.$title.'</div>';
    $html .= '<div class="panel-price" style="text-shadow: 1px 1px 2px '.$table_color.';background:'.$table_color.';color:'.$text_color.'">'.$table_price.'<div class="month">'.$per_price.'</div></div>';
    $html .= '<div class="panel-body">'.$content.'</div>';
    
    if($enable_button === 'true'){
        $html .= '<div class="panel-footer"><a href="'.$link.'" class="btn btn-default">'.$btn_value.'</a></div>';
    }
    $html .= '</div>';

    return $html;
}

add_shortcode('_pricing_table', '_pricing_tables');
