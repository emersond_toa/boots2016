<?php

function client_logo($atts, $content = null) {
    extract(shortcode_atts(array(
        'title' => '',
        'logo_items' => '3',
        'enable_box' => '0',
        'logo_images' => ''
                    ), $atts));
    $logo_images = explode(",", $logo_images);
    $html = '';
    if ($title) {
        $html .= '<h2 class="client-logo-title">' . $title . '</h2>';
    }
    $html .= '<div class="client-logos" style="display:none;" data-item="' . $logo_items . '">';
    foreach ($logo_images as $row) {
        if ($row != '') {
            if($enable_box == 'true'){
                $html .= '<a href="'.wp_get_attachment_url( $row ).'" class="swipebox">';
            }
            $html .= wp_get_attachment_image($row, 'medium');
            if($enable_box == 'true'){
                $html .= '</a>';
            }
        }
    }
    $html .= '</div>';
    return $html;
}

add_shortcode('_client_logo', 'client_logo');
