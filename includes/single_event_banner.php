<?php

function _single_event_banner($atts, $content = null) {
    extract(shortcode_atts(array(
        'main_image' => '',
        'add_title' => '',
        'add_url' => ''
                    ), $atts));
    $html = '';
    $html .= '<div class="_event_container">';
    $html .= '<div class="_event-icon">';
    $html .= wp_get_attachment_image($main_image, 'medium');
    $html .= '</div>';
    $html .= '<div class="_event-content">';
    if ($add_title) {
        $html .= '<h3 class="_event-title">' . $add_title . '</h3>';
    }
    $html .= '<div class="_event-text">' . $content . '</div>';
    $html .= '<div class="_event_btn"><a href="' . $add_url . '" class="btn btn-info" target="_blank">READ MORE</a> ';
    $html .= '<div class="pull-right">';
    $html .= ' <a href="https://www.facebook.com/sharer/sharer.php?u=' . $add_url . '" class="btn-facebook btn btn-social-icon" target="_blank"><i class="fa fa-facebook"></i></a>';
    $html .= ' <a href="http://twitter.com/share?text=' . $content . '&url=' . $add_url . '" class="btn btn-social-icon btn-twitter" target="_blank"><i class="fa fa-twitter"></i></a>';
    $html .= ' <a href="https://plus.google.com/share?url=' . $add_url . '" class="btn btn-social-icon btn-google" target="_blank"><i class="fa fa-google"></i></a>';
    $html .= ' <a href="http://www.linkedin.com/shareArticle?mini=true&url=' . $add_url . '&title=' . $add_url . '&summary=' . $content . '&source=' . $add_title . '" class="btn btn-social-icon btn-linkedin" target="_blank"><i class="fa fa-linkedin"></i></a>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    $html .= '</div>';
    return $html;
}

add_shortcode('single_event_banner', '_single_event_banner');



