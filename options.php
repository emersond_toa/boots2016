<?php

/**
 * A unique identifier is defined to store the options in the database and reference them from the theme.
 */
function optionsframework_option_name() {

    // Change this to use your theme slug
    $themename = wp_get_theme();
    $themename = preg_replace("/\W/", "_", strtolower($themename));
    $optionsframework_settings = get_option('optionsframework');
    $optionsframework_settings['id'] = $themename;
    update_option('optionsframework', $optionsframework_settings);
}

function optionsframework_options() {
    // Background Defaults
    $background_defaults = array(
        'color' => '',
        'image' => '',
        'repeat' => 'repeat',
        'position' => 'top center',
        'attachment' => 'scroll');
    // Pull all the pages into an array
    $options_pages = array();
    $options_pages_obj = get_pages('sort_column=post_parent,menu_order');
    $options_pages[''] = 'Select a page:';
    foreach ($options_pages_obj as $page) {
        $options_pages[$page->ID] = $page->post_title;
    }
    // If using image radio buttons, define a directory path
    $imagepath = get_template_directory_uri() . '/images/';
    $options = array();

    /*  Tab 1: Header Settings
      ============================================================================================ */

    $options[] = array(
        'name' => __('Header Settings', '_wp_theme'),
        'type' => 'heading');
    $options[] = array(
        'name' => __('Fixed Header', '_wp_theme'),
        'desc' => __('Do you want to fix the header?', '_wp_theme'),
        'id' => 'enable_header',
        'std' => '1',
        'type' => 'checkbox');
    $options[] = array(
        'name' => __('Top Background Color', '_wp_theme'),
        'desc' => __('Configure top background color. ', '_wp_theme'),
        'id' => 'top_bg',
        'std' => '',
        'type' => 'color');
    $options[] = array(
        'name' => __('Favicon', '_wp_theme'),
        'desc' => __('Upload a 16px by 16px PNG image that will represent your website favicon.', '_wp_theme'),
        'id' => 'favicon',
        'type' => 'upload');

    $options[] = array(
        'name' => __('Logo', '_wp_theme'),
        'desc' => __('Upload logo image for your Website. Otherwise site title will be displayed in place of logo.', '_wp_theme'),
        'id' => 'logo',
        'type' => 'upload');
    $options[] = array(
        'name' => __('Mobile / Phone', '_wp_theme'),
        'desc' => __('Add mobile or phone number', '_wp_theme'),
        'id' => 'phone_num',
        'type' => 'text');
    $options[] = array(
        'name' => __('Email', '_wp_theme'),
        'desc' => __('Add email address', '_wp_theme'),
        'id' => 'con_email',
        'type' => 'text');

    /*  Tab 2: Styling
      ============================================================================================ */
    $options[] = array(
        'name' => __('Styling', '_wp_theme'),
        'type' => 'heading');

    $options[] = array(
        'name' => __('Body Image', '_wp_theme'),
        'desc' => __('Configure body image background. ', '_wp_theme'),
        'id' => 'image_background',
        'std' => '',
        'type' => 'upload');
    $options[] = array(
        'name' => __('Background Repeat', '_wp_theme'),
        'desc' => __('Body image background repeat? ', '_wp_theme'),
        'id' => 'no_repeat',
        'std' => 'no-repeat',
        'type' => 'checkbox');

    $options[] = array(
        'name' => __('Body Text Color', '_wp_theme'),
        'desc' => __('Configure body text color. ', '_wp_theme'),
        'id' => 'body_text_color',
        'std' => '',
        'type' => 'color');

    $options[] = array(
        'name' => __('Body Link Color', '_wp_theme'),
        'desc' => __('Configure body link color. ', '_wp_theme'),
        'id' => 'body_link_color',
        'std' => '',
        'type' => 'color');

    $options[] = array(
        'name' => __('Footer Top Border Color', '_wp_theme'),
        'desc' => __('Configure footer top border color. ', '_wp_theme'),
        'id' => 'footer_top_border_color',
        'std' => '',
        'type' => 'color');

    $options[] = array(
        'name' => __('Footer Background Color', '_wp_theme'),
        'desc' => __('Configure footer background color. ', '_wp_theme'),
        'id' => 'footer_background_color',
        'std' => '',
        'type' => 'color');

    $options[] = array(
        'name' => __('Footer Bottom Background Color', '_wp_theme'),
        'desc' => __('Configure footer bottom background color. ', '_wp_theme'),
        'id' => 'footer_background_bottom_color',
        'std' => '',
        'type' => 'color');


    /*  Tab 3: Social Media
      ============================================================================================ */
    $options[] = array(
        'name' => __('Social Media', '_wp_theme'),
        'type' => 'heading');

    $options[] = array(
        'name' => __('Facebook', '_wp_theme'),
        'desc' => __('Provide facebook url.', '_wp_theme'),
        'id' => 'facebook_url',
        'std' => 'http://facebook.com',
        'type' => 'text');
    $options[] = array(
        'name' => __('Instagram', '_wp_theme'),
        'desc' => __('Provide Instagram url.', '_wp_theme'),
        'id' => 'instagram_url',
        'std' => 'http://instagram.com',
        'type' => 'text');
    $options[] = array(
        'name' => __('Twitter', '_wp_theme'),
        'desc' => __('Provide twitter url.', '_wp_theme'),
        'id' => 'twitter_url',
        'std' => 'http://twitter.com',
        'type' => 'text');

    $options[] = array(
        'name' => __('Google+', '_wp_theme'),
        'desc' => __('Provide google plus url.', '_wp_theme'),
        'id' => 'google_plus_url',
        'std' => 'http://google.com',
        'placeholder' => 'Google+',
        'type' => 'text');

    $options[] = array(
        'name' => __('YouTube', '_wp_theme'),
        'desc' => __('Provide youtube url.', '_wp_theme'),
        'id' => 'youtube_url',
        'std' => 'http://youtube.com',
        'placeholder' => 'YouTube',
        'type' => 'text');

    $options[] = array(
        'name' => __('LinkedIn', '_wp_theme'),
        'desc' => __('Provide linkedin url.', '_wp_theme'),
        'id' => 'linkedin_url',
        'std' => 'http://linkedin.com',
        'placeholder' => 'LinkedIn',
        'type' => 'text');

    $options[] = array(
        'name' => __('Pinterest', '_wp_theme'),
        'desc' => __('Provide pinterest url.', '_wp_theme'),
        'id' => 'pinterest_url',
        'std' => 'http://pinterest.com',
        'placeholder' => 'Pinterest',
        'type' => 'text');
    /*  
      ============================================================================================ */
    $options[] = array(
        'name' => __('Footer Settings', '_wp_theme'),
        'type' => 'heading');
    $options[] = array(
        'name' => __('Enable Footer Widgets', '_wp_theme'),
        'desc' => __('Do you want to enable the Footer Widgets', '_wp_theme'),
        'id' => 'footer_widgets',
        'std' => '1',
        'type' => 'checkbox');
    $options[] = array(
        'name' => __('Footer Script', '_wp_theme'),
        'id' => 'footer_script',
        'std' => '',
        'type' => 'textarea');
    $options[] = array(
        'name' => __('Addional footer Text', '_wp_theme'),
        'id' => 'footer_text',
        'std' => '',
        'type' => 'textarea');
    $options[] = array(
        'name' => __('Footer Copyright Text', '_wp_theme'),
        'id' => 'footer_copyright',
        'std' => '&copy; Copyright 2016 _wp. All Rights Reserved.',
        'type' => 'text');
    /*  Tab 5: Other Settings
      ============================================================================================ */
    $options[] = array(
        'name' => __('Other Settings', '_wp_theme'),
        'type' => 'heading');

    $options[] = array(
        'name' => __('Enable/Disable Comment', '_wp_theme'),
        'desc' => __('Do you want to enable comment?', '_wp_theme'),
        'id' => 'enable_comment',
        'std' => '1',
        'type' => 'checkbox');
    /* Enable/Disable Scroll to Top Feature */
    $options[] = array(
        'name' => __('Enable Scroll to Top ', '_wp_theme'),
        'desc' => __('Do you want to enable scroll to top feature?', '_wp_theme'),
        'id' => 'enable_scroll_to_top',
        'std' => '1',
        'type' => 'checkbox');
    return $options;
}
