<?php get_header(); ?>
<div class="container ">
    <div class="row grid-content">
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                <div class="col-md-4 col-sm-6 col-xs-12 item-grid">
                    <div class="panel panel-default"> 
                        <?php if (get_the_post_thumbnail() != ''): ?>
                            <div class="user-thumbnail">
                                <?php $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array(720, 405), false, ''); ?>
                                <a class="swipebox" href="<?php echo $src[0]; ?>">
                                    <?php the_post_thumbnail('blog-page'); ?>
                                </a>
                            </div>
                        <?php endif; ?>
                        <div class="panel-heading">
                            <?php the_title(); ?>
                        </div>
                        <div class="panel-body">
                            <?php
                            $categories_list = get_the_category_list(__(', '));
                            $tags_list = get_the_tag_list('', __(', '));
                            if ($categories_list or $tags_list) {
                                echo '<div class="meta-tags-categories">';
                                if ($categories_list) {
                                    echo '<i class="fa fa-folder"></i> ' . $categories_list;
                                }
                                if ($tags_list) {
                                    echo ' <i class="fa fa-tag"></i> ' . $tags_list;
                                }
                                echo '</div>';
                            }
                            ?>
                            <?php echo limit_text(get_the_content(), 50); ?>
                            <div class="read-more-btn">
                                <?php
                                if (of_get_option('top_bg') != '') {
                                    $readmore = ' read-more';
                                }else{
                                    $readmore = ' btn-info';
                                }
                                ?>
                                <a href="<?php the_permalink(); ?>" class="btn<?php echo $readmore ?>">Read More <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <?php require dirname(__FILE__) . '/includes/social-media.php'; ?>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        
         <?php else: ?>
        
<div class="col-md-12">
    <img src="<?php echo get_template_directory_uri() ?>/images/not-found.png" style="display: block; margin: 0 auto"/>
    <h1 class="text-center">
         0 Search Results for: du <?php echo get_search_query(); ?>
    </h1>
    <h3 class="text-center">
       The page you requested could not be found. Try refining your search, or use the navigation below to get back to home page.
    </h3>
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <form role="search" class="widget-search" method="get"  action="<?php echo esc_url(home_url('/')); ?>">
                <div class="input-group">
                    <input type="text"  value="<?php echo get_search_query(); ?>" name="s" class="form-control"  placeholder="Search">
                    <span class="input-group-btn">
                        <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                    </span>
                </div>
            </form> 
        </div>
        <div class="col-md-3"></div>
    </div>

    <div class="text-center" style="margin-top: 15px;margin-bottom: 15px;">
        <a href="<?php echo home_url(); ?>" class="btn btn-default">GET ME BACK TO HOME PAGE</a>
    </div> 
</div>
        
        
        <?php endif; ?>
    </div>
    <div class="navigation" style="display: none;">
        <ul class="row">
            <li class="col-xs-6"><?php echo get_previous_posts_link() ?></li>
            <li class="col-xs-6"><span class="pull-right"><?php echo get_next_posts_link() ?></span></li>
        </ul>    
    </div>    
</div> 
<?php
get_footer();
