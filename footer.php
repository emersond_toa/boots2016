

<footer>
    <div class="footer">
        <div class="container">
            <?php if (of_get_option('footer_widgets')): ?>
                <div class="footer-widgets">
                    <div class="row">
                        <div class="col-md-3">
                            <?php if (!dynamic_sidebar('Footer-One')) : ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-3">
                            <?php if (!dynamic_sidebar('Footer-Two')) : ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-3">
                            <?php if (!dynamic_sidebar('Footer-Three')) : ?>
                            <?php endif; ?>
                        </div>
                        <div class="col-md-3">
                            <?php if (!dynamic_sidebar('Footer-Four')) : ?>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>  
        <div class="copyright">
            <div class="container">   
                <?php echo of_get_option('footer_copyright'); ?>
            </div>
        </div>
    </div>
</footer>
</div>
<div class="modal fade search-box" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-body sidebar-widget" style="margin-bottom: 0px">
                <form role="search" class="modal-search" method="get" action="<?php echo home_url() ?>" >
                    <div class="input-group">
                        <input type="text" value="" name="s" class="form-control" placeholder="Search">
                        <span class="input-group-btn">
                            <button class="btn btn-default" type="submit"><i class="fa fa-search"></i></button>
                        </span>
                    </div>
                </form>
            </div>     
        </div>
    </div>
</div>
<?php wp_footer(); ?>
<?php 
if(of_get_option('enable_scroll_to_top')){
    echo "<a href='#' class='bcktop fa fa-arrow-up'></a>";
} 
?>
<?php echo of_get_option('footer_script'); ?>

<?php echo of_get_option('footer_text'); ?>
</body>
</html>
