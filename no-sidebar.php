<?php
/**
 * Template Name: No sidebar page
 *
 *
 */
get_header();
?>
<div class="container no-sidebar">
    <div class="row">   

        <div id="post-<?php the_ID(); ?>" <?php post_class('post-page'); ?> data=' <?php echo get_post_type() ?>'>
            <?php while (have_posts()) : the_post(); ?>
                <?php the_content(); ?>
                <?php
                wp_link_pages(array(
                    'before' => '<div class="page-links">' . __('Pages:', '_wp_theme'),
                    'after' => '</div>',
                ));
                ?>
            <?php endwhile; ?>
        </div>
    </div>
</div>
get_footer();
