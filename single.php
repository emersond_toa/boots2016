<?php get_header(); ?>
<div class="container single-page">
    <div class="row <?php echo get_post_type(); ?>">
        <div class="col-sm-8">
            <div id="post-<?php the_ID(); ?>" <?php post_class('panel panel-default'); ?> >
                <?php if (get_post_type() !== 'client-comment') { ?>
                    <?php if ((get_the_post_thumbnail() != '')): ?>
                        <div class="user-thumbnail">
                            <?php $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array(720, 405), false, ''); ?>
                            <a class="swipebox" href="<?php echo $src[0]; ?>">
                                <?php the_post_thumbnail('blog-page'); ?>
                            </a>
                        </div>
                    <?php endif; ?>
                <?php } ?>
                <div class="panel-heading">
                    <?php if (get_post_type() === 'client-comment') { ?>
                        <?php if ((get_the_post_thumbnail() != '')): ?>
                            <div class="user-thumbnail">
                                <?php $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array(720, 405), false, ''); ?>
                                <a class="swipebox" href="<?php echo $src[0]; ?>">
                                    <?php the_post_thumbnail('blog-page'); ?>
                                </a>
                            </div>
                        <?php endif; ?>
                    <?php } ?>
                    <?php the_title(); ?>
                </div>
                <div class="panel-body">
                    <?php
                    $categories_list = get_the_category_list(__(', '));
                    $tags_list = get_the_tag_list('', __(', '));
                    if ($categories_list or $tags_list) {
                        echo '<div class="meta-tags-categories">';
                        if ($categories_list) {
                            echo '<i class="fa fa-folder"></i> ' . $categories_list;
                        }
                        if ($tags_list) {
                            echo ' <i class="fa fa-tag"></i> ' . $tags_list;
                        }
                        echo '</div>';
                    }
                    ?>
                    <?php while (have_posts()) : the_post(); ?>
                        <div class="cntn-page">
                            <?php the_content(); ?>
                        </div>   
                        <?php comments_template(); ?>
                    <?php endwhile; ?>
                </div>
                <div class="panel-footer">
                    <?php require dirname(__FILE__) . '/includes/social-media.php'; ?>
                </div>
            </div>
        </div>
        <div class="col-sm-4">
            <?php get_sidebar(); ?>
        </div>
    </div>
</div>
<?php
get_footer();
