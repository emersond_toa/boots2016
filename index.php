<?php get_header(); ?>
<div class="container ">
    <div class="row grid-content">
        <?php if (have_posts()) : ?>
            <?php while (have_posts()) : the_post(); ?>
                <div class="col-md-4 col-sm-6 col-xs-12 item-grid">
                    <div class="panel panel-default"> 
                        <?php if (get_the_post_thumbnail() != ''): ?>
                            <div class="user-thumbnail">
                                <div class="blog-date"><p class="day"><?php the_time('j') ?></p><p class="monthyear"><?php the_time('M, Y') ?></p></div>
                                <?php $src = wp_get_attachment_image_src(get_post_thumbnail_id($post->ID), array(720, 405), false, ''); ?>
                                <a class="swipebox" href="<?php echo $src[0]; ?>">
                                    <?php the_post_thumbnail('blog-page'); ?>
                                </a>
                            </div>
                        <?php endif; ?>
                        <div class="panel-heading">
                            <a href="<?php the_permalink(); ?>">
                                <?php the_title(); ?>
                            </a>    
                        </div>
                        <div class="panel-body">
                            <?php
                            
                            $categories_list = get_the_category_list(__(', '));
                            $tags_list = get_the_tag_list('', __(', '));
                            if ($categories_list or $tags_list) {
                                echo '<div class="meta-tags-categories"> By: ';
                                the_author_link();
                                if ($categories_list) {
                                   // echo '<i class="fa fa-folder"></i> ' . $categories_list;
                                }
                                if ($tags_list) {
                                    //echo ' <i class="fa fa-tag"></i> ' . $tags_list;
                                }
                                echo '</div>';
                            }
                            ?>
                            <?php echo limit_text(get_the_content(), 50); ?>
                            <div class="read-more-btn">
                                <?php
                                if (of_get_option('top_bg') != '') {
                                    $readmore = ' read-more';
                                }else{
                                    $readmore = ' btn-info';
                                }
                                ?>
                                <a href="<?php the_permalink(); ?>" class="btn<?php echo $readmore ?>">Read More <i class="fa fa-long-arrow-right"></i></a>
                            </div>
                        </div>
                        <div class="panel-footer">
                            <?php require dirname(__FILE__) . '/includes/social-media.php'; ?>
                        </div>
                    </div>
                </div>
            <?php endwhile; ?>
        <?php endif; ?>
    </div>
    <div class="navigation" style="display: none">
        <ul class="row">
            <li class="col-xs-6"><?php echo get_previous_posts_link() ?></li>
            <li class="col-xs-6"><span class="pull-right"><?php echo get_next_posts_link() ?></span></li>
        </ul>    
    </div>    
</div> 
<?php
get_footer();
