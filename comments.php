<?php if (of_get_option('enable_comment') == 1) { ?>
    <?php if (comments_open()) : ?>
        <hr>
        <div class="comment-content">
            <?php if ($comments) : ?>
                <ol  class="list-group">
                    <?php foreach ($comments as $comment) : ?>
                        <li id="comment-<?php comment_ID(); ?>"  class="list-group-item">
                            <?php if ($comment->comment_approved == '0') : ?>
                                <p>Your comment is awaiting approval</p>
                            <?php endif; ?>
                            <?php echo get_avatar($comment, 50); ?>    
                            <p><?php comment_text(); ?></p>
                            <cite><?php comment_type(); ?> by <?php comment_author_link(); ?> on <?php comment_date(); ?> at <?php comment_time(); ?></cite>
                        </li>
                    <?php endforeach; ?>
                </ol>
            <?php else : ?>
                <p>No comments yet</p>
            <?php endif; ?>

            <form action="<?php echo get_option('siteurl'); ?>/wp-comments-post.php" method="post" id="commentform" class="commentform">
                <?php if ($user_ID) : ?>
                    <p>Logged in as <a href="<?php echo get_option('siteurl'); ?>/wp-admin/profile.php"><?php echo $user_identity; ?></a>. <a href="<?php echo get_option('siteurl'); ?>/wp-login.php?action=logout" title="Log out of this account">Log out &raquo;</a></p>
                <?php else : ?>

                    <p>
                        <label for="author"><small>Name <?php if ($req) echo "(required)"; ?></small></label>
                        <input type="text" name="author" class="form-control" id="author" value="<?php echo $comment_author; ?>" size="22" tabindex="1" />
                    </p>
                    <p>
                        <label for="email"><small>Mail (will not be published) <?php if ($req) echo "(required)"; ?></small></label>
                        <input type="text" name="email" class="form-control" id="email" value="<?php echo $comment_author_email; ?>" size="22" tabindex="2" />
                    </p>
                    <p>
                        <label for="url"><small>Website</small></label>
                        <input type="text" name="url" class="form-control" id="url" value="<?php echo $comment_author_url; ?>" size="22" tabindex="3" />
                    </p>
                <?php endif; ?>

                <p><textarea name="comment"  class="form-control" id="comment" cols="100%" rows="10" tabindex="4"></textarea></p>
                <p><input name="submit"  class="btn btn-default" type="submit" id="submit" tabindex="5" value="Submit Comment" /></p>
                <input type="hidden" name="comment_post_ID" value="<?php echo $id; ?>" />
                <?php do_action('comment_form', $post->ID); ?>
            </form>
        </div>      
    <?php endif; ?>
<?php
}?>