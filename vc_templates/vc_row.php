<?php

$output = $elm_class = $row_style = $custom_image = $fix_video = $video_url_webm = $video_url = $removepadding = $custombgcolor = $image_repeat = $css_background = $image_url = $img_id = $img = $image_ratio = $parallax_settings = $css_slider = $b_size = '';
extract(shortcode_atts(array(
    'elm_class' => '',
    'row_style' => '',
    'video_img' => '',
    'video_url' => '',
    'video_url_webm' => '',
    'video_url_ogv' => '',
    'custombgcolor' => '',
    'custom_image' => '',
    'image_repeat' => '',
    'image_ratio' => '',
    'pattern_parallax' => '',
    'addpadding' => '',
    'removepadding' => '',
    'b_size' => '',
                ), $atts));

$elm_class = $this->getExtraClass($elm_class);
$css_class = apply_filters(VC_SHORTCODE_CUSTOM_CSS_FILTER_TAG, 'wpb_row ' . get_row_css_class() . $elm_class, $this->settings['base']);

if ($addpadding == 'yes') {
    $space = 'padding: 20px 0;';
} else {
    $space = 'padding: 0;';
}
if ($removepadding == 'yes') {
    $removepadding = ' padding: 0;';
} else {
    $removepadding = ';';
}
if ($row_style == 'bg-row') {
    $css_background = ' style="background-color:' . $custombgcolor . ';' . $removepadding . '"';
}
if ($row_style == 'image') {
    if ($b_size == 'yes') {
        $b_size = ' background-size:100%;';
    } else {
        $b_size = '';
    }
    $img_id = preg_replace('/[^\d]/', '', $custom_image);
    $image_url = wp_get_attachment_image_src($img_id, 'full');
    $image_url = $image_url[0];
    $css_background = ' style="background-image: url(' . $image_url . '); background-repeat:' . $image_repeat . ';' . $b_size . '"';
}

if ($pattern_parallax == 'yes') {
    $pattern = ' pattern_olay';
} else {
    $pattern = '';
}

if ($row_style == 'parallax') {



    if ($b_size == 'yes') {
        $b_size = ' background-size:100%;';
    } else {
        $b_size = '';
    }
    $img_id = preg_replace('/[^\d]/', '', $custom_image);
    $image_url = wp_get_attachment_image_src($img_id, 'full');
    $image_url = $image_url[0];
    $css_background = ' style="background-image: url(' . $image_url . '); background-attachment:fixed; border-bottom: none; ' . $space . $b_size . '"';
    $parallax_settings = ' data-stellar-background-ratio="' . $image_ratio . '"';
    $css_slider = ' parallax';
}
if ($row_style == 'slider') {
    $css_slider = ' slider';
    $output .= '<div class="bg-row' . $css_slider . $pattern . '"' . $css_background . '' . $parallax_settings . '>';
}
if ($row_style == 'video') {
    $img_id = preg_replace('/[^\d]/', '', $video_img);
    $image_url = wp_get_attachment_image_src($img_id, 'full');
    $image_url = $image_url[0];
    $parallax_settings = '';
    $css_background = ' style="padding: 0; overflow: hidden; background:url(' . $image_url . ');"';
    $css_slider = ' element_video';
    $fix_video = ' style="position: relative;"';
}
if ($row_style != 'slider') {
    $output .= '<div class="bg-row' . $css_slider . $pattern . '"' . $css_background . '' . $parallax_settings . '>';
    if ($row_style == 'video') {
        $output .= '<div class = "video_back">';
        $output .= '<video poster="' . $image_url . '" preload autoplay="autoplay" loop="loop">';
        $output .= '<source src="' . $video_url . '" type="video/mp4">';
        $output .= '<source src="' . $video_url_webm . '" type="video/webm; codecs=vp8,vorbis">';
        $output .= '<source src="' . $video_url_ogv . '" type="video/ogg; codecs=theora,vorbis"></video>';
        $output .= '</div>';
    }
    $output .= '<div class="container"' . $fix_video . '>';
}
$output .= '<div class="' . $css_class . '">';
$output .= wpb_js_remove_wpautop($content);
if ($row_style != 'slider') {
    $output .= '</div>';
}
$output .= '</div></div>' . $this->endBlockComment('row');
echo $output;
